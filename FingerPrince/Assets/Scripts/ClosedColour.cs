using System;
using System.Collections.Generic;
using UnityEngine;


public class ClosedColour : IEquatable<ClosedColour> {
    public static ClosedColour Red = new ClosedColour(true, false, false);
    public static ClosedColour Green = new ClosedColour(false, true, false);
    public static ClosedColour Blue = new ClosedColour(false, false, true);

    public static ClosedColour Cyan = new ClosedColour(false, true, true);
    public static ClosedColour Yellow = new ClosedColour(true, true, false);
    public static ClosedColour Magenta = new ClosedColour(true, false, true);

    public static ClosedColour Black = new ClosedColour(true, true, true);
    public static ClosedColour White = new ClosedColour(false, false, false);

    private bool R, G, B;
    private Color Brown = new Color32(139, 69, 19, 255);
    private Color Purple = new Color32(148, 0, 211, 255);


    public ClosedColour(bool r, bool g, bool b) {
        R = r;
        G = g;
        B = b;
    }

    public static ClosedColour Snap(Color that) {
        var threshold = 0.7f;
        var cc = new ClosedColour(
            that.r > threshold,
            that.g > threshold,
            that.b > threshold);
//        Debug.Log($"Snapping {that} -> {cc}");
        return cc;
    }

    public Color AsColour() {
        if (!R && !G && !B) return Color.white;
        if (R && G && B) return Color.black;


        if (R && !G && !B) return Color.red;
        if (!R && G && !B) return Color.green;
        if (!R && !G && B) return Color.blue;
        if (R && G && !B) return Brown;
        if (R && !G && B) return Purple;
        if (!R && G && B) return Color.cyan;

        throw new Exception("bad colour");

//        return new Color(R ? 255 : 0, G ? 255 : 0, B ? 255 : 0);
    }

    public ClosedColour Mix(ClosedColour that) {
        return new ClosedColour(that.R || R, that.G || G, that.B || B);
    }

    public bool Equals(ClosedColour other) {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return R == other.R && G == other.G && B == other.B;
    }

    public override bool Equals(object obj) {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((ClosedColour) obj);
    }

    public override int GetHashCode() {
        unchecked {
            var hashCode = R.GetHashCode();
            hashCode = (hashCode * 397) ^ G.GetHashCode();
            hashCode = (hashCode * 397) ^ B.GetHashCode();
            return hashCode;
        }
    }

    public static bool operator ==(ClosedColour left, ClosedColour right) {
        return Equals(left, right);
    }

    public static bool operator !=(ClosedColour left, ClosedColour right) {
        return !Equals(left, right);
    }

    public override string ToString() {
        return $"{nameof(R)}: {R}, {nameof(G)}: {G}, {nameof(B)}: {B}";
    }

    private static readonly Dictionary<ClosedColour, string> nameLUT = new Dictionary<ClosedColour, string> {
        {Red, "Red"},
        {Green, "Green"},
        {Blue, "Blue"},

        {Magenta, "Purple"},
        {Cyan, "Cyan"},
        {Yellow, "Brown"},
        
        {Black, "Black"},
        {White, "White"},
    };
    
    public string Name() {
        return nameLUT[this];
    }
    
    
}