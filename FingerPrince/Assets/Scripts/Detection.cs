using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Detection : MonoBehaviour
{
    public bool success = false;
    public bool detection;
    public float level;
    private bool play;
    private bool held = false;
    private float timer = 0f;
    private ValueAnimator valueAnimator;

    public AudioSource runeCollide, tableCollide, cauldronCollide, bowlCollide, waterCollide;
    private AudioClip runeCollideClip, tableCollideClip, cauldronCollideClip, bowlCollideClip, waterCollideClip;

    private Tutorial tutorial;
    
    void Start() {

        tutorial = FindObjectOfType<Tutorial>();
        detection = tutorial.detection;
        level = tutorial.level;
        //valueAnimator is temporary, it is just for the tutorial 
        valueAnimator = gameObject.AddComponent<ValueAnimation>().Animator;
        runeCollideClip = runeCollide.clip;
        
        tableCollideClip = tableCollide.clip;
        bowlCollideClip = bowlCollide.clip;
        cauldronCollideClip = cauldronCollide.clip;
        waterCollideClip = waterCollide.clip;
    }
 
    void Update()
    {   

        level = tutorial.level;
        detection = tutorial.detection;
        float startTime = 0;
        
        
        //Checking whether the user has held the space bar (button) over a certain period of time for the tutorial part 2
        if (detection) {
                if(level == -1){
                    
                    if( Input.GetKeyDown("space")) {
                        startTime = Time.time;
                        timer += Time.deltaTime;
                        held = true;
                    }
                    if(held){
                        timer += Time.deltaTime;
                    }

                    if(timer > 2) { //arbitrary delta time
                        held = false;
                        success=true;
                        timer = 0;
                    }
                        
                }  
        } else {
            success = false;
        }
        
        
    }

    void OnCollisionEnter(Collision that){
        //Play sound effect for different collision
        switch (that.collider.name) {
            case "desktop":
            Debug.Log("touch desktop");
                tableCollide.Play(0);
                break;
            case "cauldronShape":
                cauldronCollide.Play(0);
                break;
            case "bowl":
                bowlCollide.Play(0);
                break;
            case "liquid":
                waterCollide.Play(0);
                break;
        }
    }
    private void OnTriggerEnter(Collider collider){
        //Play sound effect for different collision
        switch (collider.name) {
            case "SphereCollider":
                runeCollide.volume = 0.3f;
                runeCollide.Play(0);
                break;
            case "CubeCollider": 
                runeCollide.volume = 0.3f;
                runeCollide.Play(0);
                break;
        }
        //Check whether the player touches the runes to spawn object for tutorial part 1
        if(detection && (level == -2)) {
            if(collider.name == "CubeCollider" || collider.name == "SphereCollider"){
                success = true;
            }
        } else {
            success=false;
        }
        
    }

}
