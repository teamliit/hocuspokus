using System.Collections;
using System.Collections.Generic;
using libfluid;
using UnityEngine;
using Optional;

// The backing logic of any of the ingredient. This would be added to the actual ingredient.
public class Ingredient : MonoBehaviour
{

    public bool inGame = false;

    public Recipe.Feel feel;
    public Recipe.Texture texture;
    public Recipe.Shape shape;
    public Option<ClosedColour> colour;
    public Vector3 initPos;
    public AudioSource tableCollide, cauldronCollide, bowlCollide, waterCollide;
    private AudioClip tableCollideClip, cauldronCollideClip, bowlCollideClip, waterCollideClip;

    private FluidAggregateQuery query;
    
    
    void Start(){
        initPos = transform.position;
        query = gameObject.AddComponent<FluidAggregateQuery>();
    }
    
    void Update(){

        //if outside table zone, then return to top with colour initialised 
        if (inGame){
            if (transform.position.x >= 60f || 
                transform.position.x <= -60f ||
                transform.position.z >= 50f ||
                transform.position.z <= -30f)
            {   
                Instantiate(Resources.Load<ParticleSystem>("ParticleSys"), gameObject.transform.position, Quaternion.identity);
                transform.position = initPos;
                transform.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
                transform.GetComponent<Rigidbody>().angularVelocity = new Vector3(0,0,0);
                this.gameObject.GetComponent<Renderer>().material.color = Color.white;
                inGame = false;
            }
        }

        query.Colour.MatchSome(c => {
            if (query.Neighbours.Exists(N => N >= 5))
                ApplyFluid(ClosedColour.Snap(c));
        });
        
        
    }
    public void ApplyFluid(ClosedColour colour)
    {   Debug.Log("colour to be coloured on the object: "+colour);
        this.colour = this.colour.Map(existing => existing.Mix(colour)).Or(colour);
        this.colour.MatchSome(x => { GetComponent<Renderer>().material.color = x.AsColour(); });
        Debug.Log("object's colour: "+ gameObject.GetComponent<Renderer>().material.color);
    }

    void OnCollisionEnter(Collision that)
    {
//        that.gameObject.GetComponent<Fluid>()
//        .SomeNotNull()
//        .MatchSome(fluid => ApplyFluid(fluid.colour));

        switch (that.collider.name) {

            case "desktop":
                tableCollide.Play(0);
                Debug.Log("TABLE COLLIDE");
                break;
            case "bowl":
                bowlCollide.Play(0);
                break;
            case "cauldronShape":
                cauldronCollide.Play(0);
                break;
            case "liquid":
                waterCollide.Play(0);
                break;
        }
    }

    public override string ToString()
    {
        return $"[GameObject]Ingredient({feel}, {texture}, {shape}, {colour})";
    }

    //  public static GameObject AddIngredient(Recipe.Shape shape, Recipe.Texture texture, Recipe.Feel feel, Vector3 pos)
    public static GameObject AddIngredient(Recipe.Shape shape, Vector3 pos)
    {

        PrimitiveType that;
        switch (shape)
        {
            case Recipe.Shape.Cube: that = PrimitiveType.Cube; break;
            case Recipe.Shape.Sphere: that = PrimitiveType.Sphere; break;
            default: that = PrimitiveType.Cube; break;
        }

        GameObject obj = GameObject.CreatePrimitive(that);
        var ingredient = obj.AddComponent<Ingredient>();
       
        ingredient.shape = shape;
        // ingredient.texture = texture;
        // ingredient.feel = feel;
        obj.transform.position = pos;

        Rigidbody rb = obj.AddComponent<Rigidbody>();
        rb.useGravity = true;
        return obj;
    }
}