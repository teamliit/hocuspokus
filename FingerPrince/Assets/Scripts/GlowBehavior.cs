using UnityEngine;

public class GlowBehavior : MonoBehaviour{

    private float start = -1;
    private float intensity = 1;
    private readonly Color color;
    public Renderer _renderer;
    public float maxIntensity;
    public GlowBehavior(Renderer _renderer, float maxIntensity){
        this._renderer = _renderer;
        this.color = _renderer.material.color;
        this.maxIntensity = maxIntensity;
    } 
    public void Trigger(){
        start = Time.time;
        intensity = maxIntensity;
    }

    public void Update(){
        if(start != -1){
			if(Time.time - start < 0.5){
				intensity -= 0.05f;
                if(intensity <= 1) intensity = 1;
			}else{
				start = -1;
				intensity = 1;
			}
		}
		_renderer.material.color = color * intensity;
    }


}