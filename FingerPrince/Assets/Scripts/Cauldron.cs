using System.Collections;
using System.Collections.Generic;
using libfluid;
using UnityEngine;
using Optional;

// The backing logic of any of the cauldron. This would be added to the actual cauldron.
public class Cauldron : MonoBehaviour
{
    private GameManager gameManager;
    public AudioSource success, failure, background; // Set these in editor
    private AudioClip successClip, failureClip, backgroundClip;
    public bool completed = false;

    public float delayTime; //delay time before deciding if the ingredient is correct or not
    public FlyOutMethods.MethodNames wayOfFlyingOut;
    private ValueAnimator animator;
    private FluidSource source;
    private GameObject fluidEmitter;

    void Start() {

        Random.InitState((int)System.DateTime.Now.Ticks);

        gameManager = FindObjectOfType<GameManager>();

        successClip = success.clip;
        failureClip = failure.clip;
        backgroundClip = background.clip;
        
        
        animator = gameObject.AddComponent<ValueAnimation>().Animator;

        fluidEmitter = new GameObject();
        source = fluidEmitter.AddComponent<FluidSource>();
        source.velocity = new Vector3(0, 1f, 0);
        source.rate = 0;
    }

    void PlaySuccess(){
        success.PlayOneShot(successClip);
    }

    void PlayFailure(){
        failure.PlayOneShot(failureClip);
    }

    private void CorrectIngredient(Collider that, Recipe.Requirement req) {
        Debug.Log("Correct ingredient");
        gameManager.FulfilledReq(req);
        PlaySuccess();

        source.colour = req.colour.AsColour();
        fluidEmitter.transform.position = that.gameObject.transform.position;

        //place the object back to its original position; change its colour back to white
        that.gameObject.GetComponent<Renderer>().material.color = Color.white;
        that.gameObject.transform.position = that.GetComponent<Ingredient>().initPos;
        
        animator.Untagged(25, 0, 1.35f)
            .EvaluateWith(v => { source.rate = (long) v; })
            .Submit(() => {
                // just in case the last frame was skipped because of performance issues
                source.rate = 0;
                
            });
       

    }


    private void AddFlyingOutScripts(Collider that)
    {
        //Adding script accordingly to the chosen method
        if (wayOfFlyingOut == FlyOutMethods.MethodNames.FloatThenShoot)
        {
            that.gameObject.AddComponent<FloatThenShoot>();
        }
        else if (wayOfFlyingOut == FlyOutMethods.MethodNames.VibrateOut)
        {
            that.gameObject.AddComponent<VibrateOut>();
        }
        else if (wayOfFlyingOut == FlyOutMethods.MethodNames.ElasticShoot)
        {
            that.gameObject.AddComponent<ElasticShoot>();
        }
    }

    // Needs to be called in fixed update instead - why?
    // Needs different methods of flying out (including elastic model)
    private void WrongIngredient(Collider that) {
        Debug.Log("Incorrect ingredient");

        if (!that.gameObject.GetComponent<FlyOutMethods>()) {      
           
            //If choose "ChooseRandomMethod", it will automaticly pick a method for you
            //Will be different for each object
            if (wayOfFlyingOut == FlyOutMethods.MethodNames.ChooseRandomMethod)
            {
                wayOfFlyingOut = (FlyOutMethods.MethodNames)Random.Range(1, 4);
                AddFlyingOutScripts(that);
                wayOfFlyingOut = FlyOutMethods.MethodNames.ChooseRandomMethod;
            }
            else AddFlyingOutScripts(that);
        }

        PlayFailure();
    }

    private void DecideOnIngredient(Collider that) {
        // Currently only removes objects with an ingredient component
        that.gameObject.GetComponent<Ingredient>()
            .SomeNotNull()
            .MatchSome(ingredient =>
            {
                gameManager.currentRecipe().MatchSome(recipe => {
                recipe.Fulfill(ingredient)
                    .Match(
                        some: x =>
                        {
                            CorrectIngredient(that, x);
                        },
                        none: () =>
                        {
                            WrongIngredient(that);
                        }
                    );
                });
            });
    }

    // Make some wait time before a decision is made for if the ingredient is correct or not
    IEnumerator DelayThenDecide(Collider that, float delayTime)
    {
        yield return new WaitForSecondsRealtime(delayTime);
        DecideOnIngredient(that);
    }

    
    void OnTriggerStay(Collider that)
    {
        if ( GameObject.FindObjectOfType<Tutorial>().detection && GameObject.FindObjectOfType<Tutorial>().level == 0){
            that.gameObject.transform.position = that.GetComponent<Ingredient>().initPos;
            that.gameObject.GetComponent<Renderer>().material.color = Color.white;
            PlaySuccess();
            completed = true;
            Debug.Log(completed);
            
        } else if (GameObject.FindObjectOfType<Tutorial>().level > 0) {
            StartCoroutine(DelayThenDecide(that, delayTime));
        }
    }
}





