﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableCollider : MonoBehaviour {

	private HapticProperties parentHaptics;

	// Use this for initialization
	void Start () {
		parentHaptics = transform.parent.gameObject.GetComponent<HapticProperties>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider other) {
    	Debug.Log("Entered");
	}
	
	void OnTriggerStay (Collider other) {
		Debug.Log("Staying");
	}

	void OnTriggerExit (Collider other) {
		Debug.Log("Exited");
	}
}
