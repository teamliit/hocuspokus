﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidBehaviour : MonoBehaviour
{

    private Color color;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetColor(Color c){
        this.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
        this.gameObject.GetComponent<Renderer>().material.color = c;
        this.color = c;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Ingredient>() != null) {
            Debug.Log("ingredient componenet found");
//            other.GetComponent<Ingredient>().ApplyFluid(this.color);
        }
    }
}
