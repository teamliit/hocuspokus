﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    // Start is called before the first frame update
    private Camera mainCamera;

    

    private void Awake() {
        mainCamera = Camera.main;
    }

    private void Update() {
        transform.LookAt(mainCamera.transform.position, Vector3.up);
    }
}
