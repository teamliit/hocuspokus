﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// For the second gameplay demo involving copying a sequence of musical notes
public class SequenceAutomata : MonoBehaviour {

	private static readonly IEnumerable<string> sequence = new List<string> { "C","C","G","G","A","A","G" }; 

	GameState state;
	private Queue<string> remaining;
	private bool started;

	SequenceAutomata() : base() {}

	// Use this for initialization
	void Start () {
		state = new GameState(new StartState());
		remaining = new Queue<string>(sequence); 
	}
	
	// Update is called once per frame
	void Update () {
		if(remaining.Count == 0){
			Debug.Log("You won!");
			transform.GetChild(0).gameObject.SetActive(true); // This indexing could cause problems
		}
	}

	public void RedPress() {
		// play all here
		// state.RedPress();
		Debug.Log("Red");
		started = true;
	}

	private void advanceOrReset(string note){
		// foreach ( string obj in remaining ) Debug.Log(obj);


		if(started && !remaining.Dequeue().Equals(note)){
			Debug.Log("Bad, reset" );
			transform.GetChild(1).gameObject.SetActive(true);
			remaining = new Queue<string>(sequence);
		}
		Debug.Log("Press= " +note +" Remaining= [" + string.Join(", ",remaining.ToArray())  + "]");

	}
	public void YellowPress() {
		transform.GetChild(1).gameObject.SetActive(false);
		advanceOrReset("G");
	}

	public void BluePress() {
		transform.GetChild(1).gameObject.SetActive(false);
		advanceOrReset("A");
	}

	public void VioletPress() {
		transform.GetChild(1).gameObject.SetActive(false);
		advanceOrReset("C");
	}

}

// State pattern - Based on examples from: https://www.dofactory.com/net/state-design-pattern



// Abstract State Class
abstract class State {
	public virtual void RedPress(GameState gameState) {
		gameState.State = new RedState();
	}

	public virtual void YellowPress(GameState gameState) {
		gameState.State = new StartState();
	}

	public virtual void BluePress(GameState gameState) {
		gameState.State = new StartState();
	}

	public virtual void VioletPress(GameState gameState) {
		gameState.State = new StartState();
	}

	public virtual bool Won() { return false; }

}

// Concrete State classes
class StartState : State {
	// No overrides
}

class RedState : State {
	public override void YellowPress(GameState gameState) {
		gameState.State = new YellowState();
	}
}

class YellowState : State {
	public override void YellowPress(GameState gameState) {
		gameState.State = new YellowState();
	}

	public override void BluePress(GameState gameState) {
		gameState.State = new BlueState();
	}
}

class BlueState : State {
	public override void BluePress(GameState gameState) {
		gameState.State = new BlueState();
	}

	public override void VioletPress(GameState gameState) {
		gameState.State = new WinState();
	}
}

class WinState : State {
	public override void RedPress(GameState gameState) {
		gameState.State = new WinState();
	}

	public override void YellowPress(GameState gameState) {
		gameState.State = new WinState();
	}

	public override void BluePress(GameState gameState) {
		gameState.State = new WinState();
	}

	public override void VioletPress(GameState gameState) {
		gameState.State = new WinState();
	}

	public override bool Won() {
		return true;
	}
}

// Context Class
class GameState {
	private State _state; 

	public GameState (State state) {
		this.State = state;
	}

	public State State {
		get { return _state; }
		set { _state = value; }
	}

	public void RedPress() {
		_state.RedPress(this);
	}

	public void YellowPress() {
		_state.YellowPress(this);
	}

	public void BluePress() {
		_state.BluePress(this);
	}
 
	public void VioletPress() {
		_state.VioletPress(this);
	}
	
	public bool Won() {
		return _state.Won();
	}
}