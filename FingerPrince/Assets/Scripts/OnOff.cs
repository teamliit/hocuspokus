﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface OnOff {
   void Pressed();

   void Released();
}
