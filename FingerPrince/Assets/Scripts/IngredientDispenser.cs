﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class IngredientDispenser : MonoBehaviour, OnOff
{
    
    public ParticleSystem effect;
    public GameObject button;
    public float ingredientScale = 1;
    public HapticMaterial material;
    private int level = 0; // this might need to be public and get from tutorial if we are planning to do more than one level
    public GameObject attractor;
    private Vector3 spawnPoint, scale;
    private Quaternion rotation;
    private Recipe.Shape shape;
    private Transform ingredients;
    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = transform.position;
        
        if (button.GetComponent<ButtonBehaviour>().shape == Recipe.Shape.Cube){
            ingredients = GameObject.FindWithTag("Cubes").transform;
        } else {
            ingredients = GameObject.FindWithTag("Spheres").transform;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnOff.Pressed(){

        //Finding the next available cube/sphere to be spawned
        bool found = false;
        int i = 0;
        while(!found && i < (ingredients.childCount)) {
            if(!ingredients.GetChild(i).gameObject.GetComponent<Ingredient>().inGame) {
                found = true;
                ingredients.GetChild(i).gameObject.GetComponent<Ingredient>().inGame = true;
            } else {
                i++;
            }
        }

        if (found){
            var thingToSpawn = ingredients.GetChild(i).gameObject;
            Vector3 random = new Vector3(Random.Range(-2.0f, 2.0f), 0, Random.Range(-2.0f, 2.0f));
            thingToSpawn.transform.position = spawnPoint + random;

            var rb = thingToSpawn.AddComponent<RevealBehavior>();
            rb.effect = effect;
        }

    }
    void OnOff.Released(){
        
    }
}
