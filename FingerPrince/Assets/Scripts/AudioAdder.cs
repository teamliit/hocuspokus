﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAdder : MonoBehaviour
{
    // Start is called before the first frame update
    AudioSource tCollide, cCollide, bCollide, wCollide;
    AudioClip knockTable, knockCauldron, knockBowl, knockWater;
    void Start()
    {

        tCollide = this.gameObject.AddComponent<AudioSource>();
        cCollide = this.gameObject.AddComponent<AudioSource>();
        bCollide = this.gameObject.AddComponent<AudioSource>();
        wCollide = this.gameObject.AddComponent<AudioSource>();

        knockTable = Resources.Load<AudioClip>("knock_table");
        tCollide.clip = knockTable;
        
        this.gameObject.GetComponent<Ingredient>().tableCollide = tCollide;

        knockCauldron = Resources.Load<AudioClip>("knock_cauldron");
        cCollide.clip = knockCauldron;
        cCollide.pitch = 0.35f;
        this.gameObject.GetComponent<Ingredient>().cauldronCollide = cCollide;

        knockBowl = Resources.Load<AudioClip>("knock_bowl");
        bCollide.clip = knockBowl;
        this.gameObject.GetComponent<Ingredient>().bowlCollide = bCollide;

        knockWater = Resources.Load<AudioClip>("knock_water");
        wCollide.clip = knockWater;
        this.gameObject.GetComponent<Ingredient>().waterCollide = wCollide;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
