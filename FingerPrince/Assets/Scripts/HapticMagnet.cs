﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticMagnet : MonoBehaviour
{

    public static HapticMaterial copyHapticMaterial(HapticMaterial that)
    {
        var copied = (HapticMaterial)ScriptableObject.CreateInstance("HapticMaterial");
        copied.Stiffness = that.Stiffness;
        copied.HasSurface = that.HasSurface;
        copied.StaticFriction = that.StaticFriction;
        copied.DynamicFriction = that.DynamicFriction;
        copied.TextureLevel = that.TextureLevel;
        copied.MagneticDistance = that.MagneticDistance; 
        copied.MagneticForce = that.MagneticForce;
        copied.Viscosity = that.Viscosity;
        copied.SticksplipStiffness = that.SticksplipStiffness;
        copied.SticksplipForce = that.SticksplipForce;
        copied.VibrationFreq = that.VibrationFreq;
        copied.VibrationAmplitude = that.VibrationAmplitude;
        return copied;
    }

    private static string TAG = "animated";

    // Set to template HapticMaterial in editor, then mutated to the copied version
    public HapticMaterial mat; 

    private ValueAnimator animator;
    private Magnetic magnetic;
    private HapticShape shape;
    private double maxForce;

    void Start()
    {   
        animator = gameObject.AddComponent<ValueAnimation>().Animator;
        
        // Script for unity forces
        magnetic = gameObject.GetComponent<Magnetic>();
        magnetic.enabled = false;

        // Make a copy of the HapticMaterial
		    mat = copyHapticMaterial(mat); 
        maxForce = mat.MagneticForce;

        // HapticShape needed for dynamic TouchableObjects
        shape = gameObject.AddComponent<HapticShape>();
        shape.material = mat; 

        // Start with objects demagnetised
        mat.MagneticForce = 0; 
        mat.OnValidate();
    }

    public void demagnitise()
    {
        // Cancel whatever animation is currently running
        animator.Cancel(TAG);
        // Disable Unity forces
        magnetic.enabled = false;
        // Create a new value animation that gently lowers the 
        // magnetic force on the object to 0, easing both in and out
        animator.Tagged(TAG, (float)mat.MagneticForce, 0, 0.8f).EvaluateWith(f =>
        {
            mat.MagneticForce = f;
            mat.OnValidate();
        }, Interpolators.EASE_BOTH).Submit();
    }

    public void magnitise()
    {
        // Cancel whatever animation is currently running
        animator.Cancel(TAG);
        // Enable unity forces
        magnetic.enabled = true;
        // Create a new value animation that gently increases the 
        // magnetic force on the object to the max force, easing both in and out
        animator.Tagged(TAG, (float)mat.MagneticForce, (float)maxForce, 2).EvaluateWith(f =>
        {
            mat.MagneticForce = f;
            mat.OnValidate();
        }, Interpolators.EASE_BOTH).Submit();
    }


    void OnDestroy()
    {
        // Stop animation if object destroyed, 
        // otherwise null reference errors will happen
        animator.Cancel(TAG);
    }

}
