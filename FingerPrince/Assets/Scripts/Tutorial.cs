using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Optional;

// The backing logic of the scroll. This would be added to the actual scroll model.  
public class Tutorial : MonoBehaviour {

    //public readonly List<Recipe.Requirement> requirements;
    public IList<Recipe.Requirement> requirements;
    private GameManager gameManager;
    private ValueAnimator valueAnimator;
    public float level = -2;
    private Vector3 savedRotation;
    private bool front = false;
    private GameObject scroll;
    private Rigidbody body;
    private GameObject plane;
    public ParticleSystem effect;
    private bool play = false;  //allow the tutorialScreen to run
    public bool detection = false;
    public bool success, completed;  //Indication from other scripts to tell tutorial that each part is fulfilled
    private Transform savedTransform;
    private GameObject canvasGO;
    private GameObject cardCanvas;
    private GameObject cauldron;
    void Start(){
        scroll = this.gameObject;
        valueAnimator = gameObject.AddComponent<ValueAnimation>().Animator;
        gameManager = FindObjectOfType<GameManager>();
        gameManager.currentRecipe().MatchSome(recipe => 
           { requirements = new List<Recipe.Requirement>( recipe.todos); }
        );

        body = scroll.AddComponent<Rigidbody>();
		body.mass = 1f;
		body.drag = 0f;
		body.angularDrag = 0.05f;
		body.collisionDetectionMode = CollisionDetectionMode.Discrete;
		body.useGravity = false;
		body.constraints = RigidbodyConstraints.FreezeRotation;

        // Rotation has to be set to fixed angle then changed back because Unity sucks
        savedRotation = this.gameObject.transform.eulerAngles;
        this.gameObject.transform.eulerAngles = new Vector3(0,90,-90);
        savedTransform = scroll.transform;
        success = GameObject.FindObjectOfType<Detection>().success;
        completed = GameObject.FindObjectOfType<Cauldron>().completed;
        createCanvas(scroll);
        flyInAnim(scroll);
        cauldron = GameObject.FindWithTag("cauldron");
    }

    void Update(){
        success = GameObject.FindObjectOfType<Detection>().success;
        completed = cauldron.GetComponentInChildren<Cauldron>().completed;

        //levels from -2 to 0 are tutorial
        if (level == -2 && play) {
            play = false;
            detection = true;
            cardPopup("TOUCH the rune to spawn objects");
        } else if (level == -1 && play) {
            play = false;
            detection = true;
            cardPopup("HOLD SPACE to levitate objects");
        } else if (level == 0 && play) {
            play = false;
            detection = true;
            cardPopup("Place an object into the cauldron");
        }
        if (level < 1 && detection && (success || completed)) {
            detection = false;
            level++;
            play = true;
            Destroy(cardCanvas);
        }

        //level > 0 is where the actual game starts
        if (level > 0 && play) {
            play = false;
            var cg = canvasGO.GetComponent<CanvasGroup>();
            valueAnimator.Untagged(1, 0, 2).EvaluateWith(f => {
                cg.alpha = f;
            }).Submit(()=>{clearText(); flyInAnim(scroll);});
        }
    }
    private void flyInAnim(GameObject scroll){
        front = true;
        //translation
        Vector3 objPos = scroll.transform.position;
        float tx = 45f;
        float ty = 5f;
        float tz = -25f;
        
        valueAnimator.Untagged(0, 1, 2).EvaluateWith(f => {
            var x = objPos.x + f*tx;
            var y = objPos.y + f*ty;
            var z = objPos.z + f*tz;
            scroll.transform.position = new Vector3(x, y, z);
        }, Interpolators.EASE_BOTH).Submit();

        //rotation
        Vector3 objRot = savedRotation;
        float ry = 40;
        float rY;
        valueAnimator.Untagged(0, 1, 2).EvaluateWith(f => {
            rY = objRot.y + f*ry;
            scroll.transform.eulerAngles = new Vector3(objRot.x, rY, objRot.z);
        }, Interpolators.EASE_BOTH).Submit(()=>{
            tutorialScreen();
        });
     
        
    }
    private void flyOutAnim(GameObject scroll){
        front = false;
        //rotation
        Vector3 objRot = scroll.transform.eulerAngles;
        float ry = 40;
        float rY;
        valueAnimator.Untagged(0, 1, 2).EvaluateWith(f => {
            rY = objRot.y - f*ry;
            scroll.transform.eulerAngles = new Vector3(objRot.x, rY, objRot.z);
        }, Interpolators.EASE_BOTH).Submit();

        //translation
        Vector3 objPos = scroll.transform.position;
        float tx = 45f;
        float ty = 6f;
        float tz = -25f;
        valueAnimator.Untagged(0, 1, 2).EvaluateWith(f => {
            var x = objPos.x - f*tx;
            var y = objPos.y - f*ty;
            var z = objPos.z - f*tz;
            scroll.transform.position = new Vector3(x, y, z);
        }, Interpolators.EASE_BOTH).Submit(()=>{
            if (level == -2) {
                play = true;
            } 
            if (level == 1){
                gameManager.currentRecipe().MatchSome(recipe => 
                    {recipe.startTimer();}
                );
            }
        });
    }

    //Keep the scroll in place for certain time
    private void floatAround(GameObject scroll){
        
        if(front) {
            if (level == -2) valueAnimator.Untagged(0, 0, 6).EvaluateWith(f => {}).Submit(()=>flyOutAnim(scroll));
            else if (level == 1) {
                valueAnimator.Untagged(0, 0, 10).EvaluateWith(f => {}).Submit(()=>{
                    flyOutAnim(scroll);
                });
            }
        }
        
    }

    //Initialize the tutorial instruction display
    private void cardPopup(String txt){

        GameObject plane = GameObject.FindWithTag("scoreBoard");
       
        cardCanvas = new GameObject();
        cardCanvas.name = "canvas";
        cardCanvas.AddComponent<Canvas>();
        cardCanvas.AddComponent<CanvasScaler>();
        cardCanvas.AddComponent<GraphicRaycaster>();
        cardCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        cardCanvas.GetComponent<RectTransform>().SetParent(plane.transform);
        cardCanvas.GetComponent<RectTransform>().sizeDelta = new Vector3(18f ,12f, 0);
        cardCanvas.GetComponent<RectTransform>().localEulerAngles = new Vector3(90,0,0);
        cardCanvas.GetComponent<RectTransform>().localPosition = new Vector3(0.02f, 0.003f,-0.055f);
        cardCanvas.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 100;
        
        var rb = cardCanvas.AddComponent<RevealBehavior>();
		rb.effect = effect;

        GameObject textGO = new GameObject();
        textGO.transform.parent = cardCanvas.transform;
        textGO.name = "ins1";
        Text text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = txt;
        text.fontSize = 8;
        text.lineSpacing = 0.5f; 
        text.alignment = TextAnchor.MiddleCenter;

        Color c = Color.black;
        c.a -= 0.1f;
        text.color = c ;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;
        text.GetComponent<RectTransform>().sizeDelta = new Vector3(18f, 12f, 1);
        text.GetComponent<RectTransform>().localPosition = new Vector3(0.02f,0,-0.06f);
        text.GetComponent<RectTransform>().localScale = new Vector3(1f, 0.8f, 1);
    }

    //Tutorial showed on the scroll
    public void tutorialScreen(){
        writeText("Hocus Pokus", 0, Color.black, "title");
        createLine(0.3f, true);

        if (level == -2){
            writeText("Welcome to your Magical Workshop ! " + "Use your sense of touch to explore the world around you." +
            "Good luck, and happy brewing !", 3f, Color.black, "ins");

        } else if (level == 1){
            writeText("You have orders for magic potions which need to be fulfilled ! " + 
            "To make the potions, you should put the ingredients listed on this scroll into the cauldron.", 3f, Color.black, "ins");
        }
        var cg = canvasGO.GetComponent<CanvasGroup>();
        valueAnimator.Untagged(0, 1, 1).EvaluateWith(f => {
            cg.alpha = f;
        }).Submit(()=>{
            floatAround(scroll);
            if (level > 0) {
                valueAnimator.Untagged(0, 0, 5).EvaluateWith(f => {
                }).Submit(()=>{
                    clearText();
                    spawnTexts();
                });
                
            }
        });
    }

    //show the recipe of the potion
    public void spawnTexts(){
        GameObject scroll = this.gameObject;
        gameManager.currentRecipe().MatchSome(recipe => 
           { 
                requirements = new List<Recipe.Requirement>( recipe.todos); 
           }
        );
        if (requirements.Count >= 1)
        {
            writeText("Potion "+ level, 0, Color.black, "title");
            createLine(0.3f, true);
            convertToText();
        }
    }

    //create a canvas for the text to show up on
    private void createCanvas(GameObject scroll){
        canvasGO = new GameObject();
        canvasGO.name = "canvas";
        canvasGO.AddComponent<Canvas>();
        var cg = canvasGO.AddComponent<CanvasGroup>();
        cg.alpha = 0;
        canvasGO.AddComponent<CanvasScaler>();
        canvasGO.AddComponent<GraphicRaycaster>();
        canvasGO.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        canvasGO.GetComponent<RectTransform>().SetParent(savedTransform);
        canvasGO.GetComponent<RectTransform>().sizeDelta = new Vector2(1, 1);
        canvasGO.GetComponent<RectTransform>().localScale = new Vector3(0.08f, 0.097f, 0);
        canvasGO.GetComponent<RectTransform>().localPosition = new Vector3(0.021f, 0.003f, -0.059f);
        canvasGO.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 250;
    }

    //putting the text on the canvas
    private void writeText( string txt, float order, Color color, string type){
        float height = 0.4f - order * 0.12f;

        GameObject textGO = new GameObject();
        textGO.transform.parent = canvasGO.transform;
        textGO.name = txt;
        Text text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = txt;
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 1;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;

        // Text position
        RectTransform rectTransform = text.GetComponent<RectTransform>();
        
        Color c = color;
        if (type == "title"){
            c.a -= 0.1f;
            text.color = c;
            text.alignment = TextAnchor.MiddleCenter;
            rectTransform.localScale = new Vector3(0.12f, 0.14f, 0.08f);
            rectTransform.localPosition = new Vector3(0, height, 0);
            rectTransform.sizeDelta = new Vector2(9, 1.2f);
        }
        else if(type=="ins"){
            c.a -= 0.1f;
            text.color = c;
            text.alignment = TextAnchor.MiddleLeft;
            text.lineSpacing = 0.8f;
            rectTransform.localScale = new Vector3(0.15f, 0.12f, 0.08f);
            rectTransform.sizeDelta = new Vector2(6, 9);
            rectTransform.localPosition = new Vector3(0.02f, height, 0);
        }
        else {
            c.a -= 0.1f;
            text.color = c;
            text.alignment = TextAnchor.MiddleCenter;
            rectTransform.localScale = new Vector3(0.13f, 0.1f, 0.08f);
            rectTransform.sizeDelta = new Vector2(6, 1.2f);
            rectTransform.localPosition = new Vector3(0, height, 0);
        } 
    }

    //underline and cross-off line
    private void createLine(float order, bool underline){
        float height = 0.4f - order * 0.12f;
        GameObject textGO = new GameObject();
        textGO.transform.parent = canvasGO.transform;
        textGO.name = "strikethrough";

        Text text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = "-";
        text.fontSize = 1;
        text.color = Color.black;
        text.alignment = TextAnchor.MiddleLeft;
        text.fontStyle = FontStyle.Bold;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;

        // Text position
        RectTransform rectTransform = text.GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(-0.02f, height-0.01f, 0);
        rectTransform.sizeDelta = new Vector2(0.4f, 1.2f);
        if (underline) {
            rectTransform.localScale = new Vector3(1.1f, 0.1f, 0.1f);
            rectTransform.localPosition = new Vector3(0.01f, height, 0);
        }
        else rectTransform.localScale = new Vector3(2.8f, 0.1f, 0.1f);
    }
    //converting the recipe ingredients into string
    private void convertToText(){
        int count = 1;
        foreach (Recipe.Requirement elem in requirements){
            string txt = elem.colour.Name() + " " + Enum.GetName(typeof(Recipe.Shape), elem.shape);
            writeText( txt, count, Color.black, "recipe");
            count += 1;
        }
    }

    //clearing the TEXT objects
    public void clearText(){
        foreach (Transform child in canvasGO.transform) {
            Destroy(child.gameObject);
        }
    }

    //called by cauldron when the correct object is thrown into the cauldron
    public void CrossOff(Recipe.Requirement that){
        int index = requirements.IndexOf(that);
        if (index != -1) createLine(index+1, false);
        else throw new Exception("Invalid requirement index");
    }

    //Scroll display when player finished all the recipes
    public void winScreen(){
        AudioSource audio = GetComponent<AudioSource>();
        AudioClip clip = audio.clip;
        audio.PlayOneShot(clip);
        
        float height = -0.008f;
        GameObject textGO;
        Text text;
        RectTransform rectTransform;

        textGO = new GameObject();
        textGO.transform.parent = canvasGO.transform;
        textGO.name = "win";
        text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = "You have now mastered the art of potions mixing. Congratulations!!";
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 20;

        Color c = Color.black;
        c.a -= 0.1f;
        text.color = c ;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;

        // Text position
        rectTransform = text.GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(0.02f, height, 0);
        rectTransform.sizeDelta = new Vector2(9, 8);

        text.alignment = TextAnchor.MiddleCenter;
        rectTransform.localScale = new Vector3(0.1f, 0.12f, 0.08f);
        rectTransform.localPosition = new Vector3(0, height, 0);
        
    }

    //Scroll display when the time is up and player has not finished all the recipes
    public void gameOver(){
        float height = -0.008f;
        GameObject textGO;
        Text text;
        RectTransform rectTransform;

        textGO = new GameObject();
        textGO.transform.parent = canvasGO.transform;
        textGO.name = "win";
        text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = "Time is up. \n Game over !";
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 20;

        Color c = Color.black;
        c.a -= 0.1f;
        text.color = c ;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;

        // Text position
        rectTransform = text.GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(0.02f, height, 0);
        rectTransform.sizeDelta = new Vector2(9, 8);

        text.alignment = TextAnchor.MiddleCenter;
        rectTransform.localScale = new Vector3(0.1f, 0.12f, 0.08f);
        rectTransform.localPosition = new Vector3(0, height, 0);
        
    }

}
