﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElasticShoot : FlyOutMethods
{
    private float magnitude = 0.8f;

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);

        Vector3 randomDir = new Vector3(Random.Range(-1,1), 1, Random.Range(-1,1)).normalized;     
        float mass = gameObject.GetComponent<Rigidbody>().mass;
        Vector3 upForce = Vector3.up * 100;

        Vector3 force = upForce + (mass * randomDir * magnitude);
        gameObject.GetComponent<Rigidbody>().AddForce(force);

        Destroy(GetComponent<ElasticShoot>());
    }
}
