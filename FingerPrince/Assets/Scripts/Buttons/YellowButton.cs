﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowButton : AudioButton {
	override public void HandleOnPressed(SequenceAutomata automata){
		automata.YellowPress(); 
	} 
}
