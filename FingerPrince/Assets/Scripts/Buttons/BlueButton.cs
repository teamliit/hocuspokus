﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueButton : AudioButton {
	override public void HandleOnPressed(SequenceAutomata automata){
		automata.BluePress(); 
	} 
}
