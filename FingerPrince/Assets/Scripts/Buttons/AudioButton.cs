using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AudioButton : MonoBehaviour   {
    // public AudioButton(){   }

	protected AudioSource MusicSource;
	private float timePressed;
	public float cooldown;

    private SequenceAutomata automata;
    private GlowBehavior glow;

	protected void Start () {
		MusicSource = GetComponent<AudioSource>();
        automata = transform.parent.gameObject.GetComponent<SequenceAutomata>();
        glow = new GlowBehavior(GetComponent<Renderer>(), 5f);
	}
	
	void Update () {
        glow.Update();
	}

    public abstract void HandleOnPressed(SequenceAutomata automata);
	void OnCollisionEnter(Collision col) {
		if (Time.time > timePressed + cooldown) {
			timePressed = Time.time;
            Debug.Log("Automata = " + automata);
			MusicSource.Play();
            glow.Trigger();
            HandleOnPressed(automata);
			// gameScript.VioletPress(); 
		}
	}
}
