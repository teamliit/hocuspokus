﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VioletButton :  AudioButton {

	override public void HandleOnPressed(SequenceAutomata automata){
		automata.VioletPress(); 
	} 
	 
}
