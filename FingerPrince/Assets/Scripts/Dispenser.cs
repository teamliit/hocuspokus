using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Optional;
using UnityEngine.Assertions;


// The backing logic of any of the dispenser. This would be added to the actual dispenser.
// The idea here is that this is a dumb object that just blocks/unblocks the nozzle so that the liquid can come out freely.   
public class Dispenser : MonoBehaviour, OnOff {


    public Color color;
    public GameObject container, liquid; 
    private IList<GameObject> targets = new List<GameObject>(); //objects whose colour will be changed
    private Renderer targetRender;


    private GameObject valve;

    void Start() {
        foreach (Material material in container.GetComponent<Renderer>().materials){
            material.color = color/2.0f;
        }
        liquid.GetComponent<LiquidBehaviour>().SetColor(color);

    }

    public void AddTarget(GameObject other) {
        Assert.IsNotNull(other.GetComponent<Ingredient>());
        targets.Add(other);
    } 

    public void RemoveTarget(GameObject other)
    {
        if(targets.Contains(other)) targets.Remove(other);
    }
    void OnOff.Pressed(){  

    }

    void OnOff.Released(){
    }

}