﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// Applies a floating motion with real physics and randomness to a stationary 3D object
public class CandleFloatMotion : MonoBehaviour {

	public float limit = 0.5f;
	private Rigidbody body;

	private Vector3 init;
	void Start () {

		init = transform.position;

		this.gameObject.AddComponent<Rigidbody>();
		body = this.gameObject.GetComponent<Rigidbody>();
		body.mass = 1f;
		body.drag = 0f;
		body.angularDrag = 0.05f;
		body.collisionDetectionMode = CollisionDetectionMode.Discrete;
		body.useGravity = false;
		body.constraints = RigidbodyConstraints.FreezeRotation;

		body.AddForce(MotionUtils.MkRandVec3(limit * 2));

	}
	 
	//Allow the objects to move around within a limit in all 3 directions
	void Update () {
		MotionUtils.RangedDeflect(init.x, limit, transform.position.x, x => body.AddForce(new Vector3(3*x, 0, 0)));
		MotionUtils.RangedDeflect(init.y, limit, transform.position.y, x => body.AddForce(new Vector3(0, 3*x, 0)));
		MotionUtils.RangedDeflect(init.z, limit, transform.position.z, x => body.AddForce(new Vector3(0, 0, 3*x)));
	}

	
}
