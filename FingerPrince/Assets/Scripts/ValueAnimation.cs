using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ValueAnimation : MonoBehaviour
{
    public ValueAnimator Animator { get; } = new ValueAnimator();

    void Update()
    {
        Animator.Tick();
    }


}