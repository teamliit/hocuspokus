﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrateOut : FlyOutMethods
{
    //Moving upward in a sin wave motion for 2s
    private Rigidbody rb;

    private float movingSpeed = 15f;
    private Vector3 movingDir;

    private float amplitude = 25f;
    private float frequency = 3f;
    private Vector3 vibratingDir;

    private float startTime;
    private float t = 0;

    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);

        rb = GetComponent<Rigidbody>();
        startTime = Time.time;

        movingDir = Vector3.up;
        vibratingDir = new Vector3(Random.value, 0, Random.value).normalized;
    }

    private void Drop()
    {
        rb.velocity = 1.5f * vibratingDir * amplitude * Mathf.Sin(frequency * t);
        Destroy(GetComponent<VibrateOut>());
    }


    void FixedUpdate()
    {
        if (t < 2f)
        {
            t = Time.time - startTime;
            rb.velocity = movingDir * movingSpeed + vibratingDir * amplitude * Mathf.Sin(frequency * t);
        }
        else Drop();
    }
}
