﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnetic : MonoBehaviour {
	
  public float magneticConstant = 80f;
	public float minDistance = 1f;
	public float maxDistance = 10f;
	public float maxForce = 50f;
	public ForceMode forceMode = ForceMode.Force;
	Rigidbody rigidBody;

  // The gameobject that the objects will be attracted to, 
  // should be the HapticTool, which is a sphere
 	public GameObject attractor; 

	void Start () {
		rigidBody = this.GetComponent<Rigidbody>();

		if (rigidBody == null) {
			Debug.LogError("Rigid body could not be found");
		}
		if (attractor == null) {
			Debug.LogError("Attractor GameObject could not be found");
		}
	}

  private float GetSphereRadius() {
		SphereCollider sphereCollider = attractor.GetComponent<SphereCollider>();
		if (sphereCollider != null) {
			return sphereCollider.radius;
		} else {
			Debug.LogError("Sphere collider could not be found");
			return 0f;
		}
	}

  // Calculate the amount of force that should be added to the object based
  // on an inverse square law (similar to magnets or gravity)
	private Vector3 CalcMagneticForce () {
    // Get difference in position vectors, 
    // which are based on the centre of mass of the objects
		Vector3 centrediff = attractor.transform.position - this.transform.position;
    
    // Subtract the part of the vector that goes 
    // from the surface of the sphere to the centre
		Vector3 diff = centrediff - (centrediff.normalized * GetSphereRadius());

    // Only apply forces to the object if they're within a particular range
		if (diff.magnitude <= maxDistance && diff.magnitude >= minDistance) {
			// Magnitude of the force is calculated according to inverse square law
      // with arbitrary constant factor
      float magnitude = magneticConstant / diff.sqrMagnitude;

      // Cap the magnitude at a particular value, then create the vector of
      // force by combining direction vector to the attractor and the magnitude
			if (magnitude <= maxForce) {
				return diff.normalized * magnitude;
			} else {
				return diff.normalized * maxForce;
			}
		} else {
			return Vector3.zero;
		}
	}

  // Apply forces in FixedUpdate so they work nicely with Unity's physics engine
	void FixedUpdate () {
		if (rigidBody != null) {
			rigidBody.AddForce(CalcMagneticForce(), forceMode);
		} else {
			Debug.Log("No RigidBody attached to this object"); 
		}
	}
}
