﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispenserDetector : MonoBehaviour
{

    private Dispenser dispenser;

    // Start is called before the first frame update
    void Start()
    {
        dispenser = this.transform.parent.GetComponent<Dispenser>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {   
        Debug.Log("OnTrigger: " + other);
        if(other.GetComponent<Ingredient>() != null){
            dispenser.AddTarget(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        dispenser.RemoveTarget(other.gameObject);
    }
}
