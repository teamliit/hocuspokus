﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBehaviour : MonoBehaviour
{
    public GameObject target;
    private float timePressed;
	  public float cooldown;
    public Recipe.Shape shape;
    private bool cooling = false;
    private Color coolDownColour = new Color32(150, 150, 150, 255);
    private Color originalColour = new Color32(75, 205, 60, 255);
    private GameObject ingredients;
    void Start()
    { 
      timePressed = -100f;
      
      //To get the corresponding set of game objects for the button
      if (shape == Recipe.Shape.Cube){
        ingredients = GameObject.FindWithTag("Cubes");
      } else {
        ingredients = GameObject.FindWithTag("Spheres");
      }
    }

    void FixedUpdate() {

      int count = 0;
      foreach(Transform ingredient in ingredients.transform){
        if(!ingredient.gameObject.GetComponent<Ingredient>().inGame) count++;
      }
      //If there are no more ingredients to spawn, the button will look disabled
      if (count==0) {
        Debug.Log("no more ingredients");
        cooling = true;
        this.transform.parent.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Vector4 (0.1f, 0.1f, 0.1f, 1);
      } 
      //To check whether the cooldown is finished for the button
      else if (cooling && Time.time > timePressed + cooldown) {
        Debug.Log("cooldown finished");
        this.transform.parent.GetChild(0).gameObject.GetComponent<Renderer>().material.color = Color.white;
        cooling = false;
      }
    }

    //Allow the buttons to spawn object only when the haptic sphere touches the button and cooldown is finished
    private void OnTriggerEnter(Collider other)
    { 
      
      if (other.name == "Haptic Sphere"){
        if (Time.time >= timePressed + cooldown && !cooling) {
          timePressed = Time.time;
          OnOff portal = target.GetComponent<IngredientDispenser>();
          if (portal != null) {
            portal.Pressed();
            this.transform.parent.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Vector4 (0.1f, 0.1f, 0.1f, 1);
            cooling = true;
          }
        }
      }
      
    }

}
