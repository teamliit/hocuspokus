using System;
using UnityEngine;

namespace libfluid {
    public class FluidDrain : MonoBehaviour {
        public float width = 1;
        public float depth = 1;

        public Drain Data(float scale) {
            return new Drain(FluidMesh.yFlipped(transform.position) * scale,
                width * scale,
                depth * scale);
        }

        void OnDrawGizmos() {
            Gizmos.color = Color.black;
            Gizmos.DrawWireCube(transform.position, Vector3.one * width);
        }
    }
}