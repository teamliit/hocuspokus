using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace libfluid {
    public class CastUtils {
        private CastUtils() { }

        public enum Axis {
            X,
            Y,
            Z
        }

        static void CastAxis(Collider target,
            int rangeA, int rangeB, float depth,
            Vector3 offset, float spacing,
            Axis axis, IList<Vector3> hits) {
            var len = depth / 2 + spacing;
            var min = -spacing;
            var max = depth + spacing;

            for (int x = -1; x <= rangeA; x++) {
                for (int y = -1; y <= rangeB; y++) {
                    Ray start, end;

                    var deltaA = x * spacing;
                    var deltaB = y * spacing;

                    switch (axis) {
                        case Axis.X:
                            start = new Ray(
                                offset + new Vector3(min, deltaA, deltaB),
                                Vector3.right * len);
                            end = new Ray(
                                offset + new Vector3(max, deltaA, deltaB),
                                Vector3.left * len);
                            break;
                        case Axis.Y:
                            start = new Ray(
                                offset + new Vector3(deltaA, min, deltaB),
                                Vector3.up * len);
                            end = new Ray(
                                offset + new Vector3(deltaA, max, deltaB),
                                Vector3.down * len);
                            break;
                        case Axis.Z:
                            start = new Ray(
                                offset + new Vector3(deltaA, deltaB, min),
                                Vector3.forward * len);
                            end = new Ray(
                                offset + new Vector3(deltaA, deltaB, max),
                                Vector3.back * len);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
                    }

                    RaycastHit hitA;
                    RaycastHit hitB;

                    if (target.Raycast(start, out hitA, len))
                        hits.Add(target.transform.InverseTransformPoint(
                            hitA.point - (hitA.normal * spacing)));
                    if (target.Raycast(end, out hitB, len))
                        hits.Add(target.transform.InverseTransformPoint(
                            hitB.point - (hitB.normal * spacing)));
//                    Gizmos.DrawWireSphere(start.origin, 0.01f);
//                    Gizmos.DrawWireSphere(end.origin, 0.01f);
//                    Gizmos.DrawRay(start);
//                    Gizmos.DrawRay(end);
                }
            }
        }

        public static IList<Vector3> Sample6D(Collider target, float spacing) {
            var bound = target.bounds;
            var size = bound.size;
            var offset = bound.min;
            var division = size / spacing;
            var bounds = new Vector3Int(
                Mathf.CeilToInt(division.x),
                Mathf.CeilToInt(division.y),
                Mathf.CeilToInt(division.z));

            var hits = new List<Vector3>();

            CastAxis(target, bounds.x, bounds.y, size.z, offset, spacing, Axis.Z, hits);
            CastAxis(target, bounds.y, bounds.z, size.x, offset, spacing, Axis.X, hits);
            CastAxis(target, bounds.x, bounds.z, size.y, offset, spacing, Axis.Y, hits);
            return hits;
        }
    }
}