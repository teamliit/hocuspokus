using System;

namespace libfluid {
    public class PinnedMonotonicArray<T> where T : struct {
        private T[] data;
        private int length;

        public T[] Data => data;
        public int Length => length;

        public PinnedMonotonicArray(int size) {
            data = new T[size];
            length = size;
        }

        public void Ensure(int size) {
            length = size;
            if (data.Length < size) {
                data = new T[size];
            }
        }

        public T[] Compacted() {
            if (length == data.Length) return Data;
            T[] dest = new T[length];
            Array.Copy(data, dest, dest.Length);
            return dest;
        }
    }
}