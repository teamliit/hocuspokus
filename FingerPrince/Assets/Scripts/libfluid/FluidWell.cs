using System;
using UnityEngine;

namespace libfluid {
    public class FluidWell : MonoBehaviour {
        public float force = 1;


        public Well Data(float scale) {
            return new Well(FluidMesh.yFlipped(transform.position) * scale, force * 1000 * 1000);
        }
    }
}