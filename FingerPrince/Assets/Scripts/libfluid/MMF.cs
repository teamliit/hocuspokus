namespace libfluid {
    using System.IO.MemoryMappedFiles;
    using System.IO;
    using System;
    using UnityEngine;

    public class MmfIO : IDisposable {
        private MemoryMappedFile file;

        public BinaryWriter Writer { get; }
        public BinaryReader Reader { get; }

        public MmfIO(string path, long lengthIfNotExist) {
//            if(!File.Exists(path)) Mmf.CreateFixedLength(path,  lengthIfNotExist);
            file = Mmf.FromPath(path);
            Writer = new BinaryWriter(file.CreateViewStream());
            Reader = new BinaryReader(file.CreateViewStream());
        }

        public void Dispose() {
            Writer?.Dispose();
            Reader?.Dispose();
            file?.Dispose();
        }
    }

    class Mmf {
        private Mmf() { }


        public static void CreateFixedLength(string path, long length) {
            using (StreamWriter sw = new StreamWriter(File.Open(path, FileMode.Append))) {
                sw.Write(new char[length]);
            }
        }

        public static MemoryMappedFile FromPath(string path) {
            var stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite,
                FileShare.ReadWrite);
            return MemoryMappedFile.CreateFromFile(
                stream,
                null,
                0,
                MemoryMappedFileAccess.ReadWrite,
                HandleInheritability.None,
                false);
        }

        public static void FlushAndReset(BinaryWriter writer) {
            writer.Flush();
            writer.Seek(0, SeekOrigin.Begin);
        }
        
        public static void FlushAndReset(BinaryReader reader) {
            reader.BaseStream.Seek(0, SeekOrigin.Begin);
        }

        public static long ReadLong(BinaryReader reader, long length) {
            switch (length) {
                case 2: return reader.ReadInt16();
                case 4: return reader.ReadInt32();
                case 8: return reader.ReadInt64();
                default: throw new Exception($"Unable to read with length:{length}");
            }
        }

        public static int ReadInt(BinaryReader reader, long length) {
            switch (length) {
                case 2: return reader.ReadInt16();
                case 4: return reader.ReadInt32();
                case 8: return (int) reader.ReadInt64();
                default: throw new Exception($"Unable to read with length:{length}");
            }
        }

        public static void WriteInt(BinaryWriter writer, long length, int value) {
            switch (length) {
                case 1:
                    writer.Write((byte) value);
                    break;
                case 2:
                    writer.Write((ushort) value);
                    break;
                case 4:
                    writer.Write(value);
                    break;
                case 8:
                    writer.Write((long) value);
                    break;
                default: throw new Exception($"Unable to write with length:{length}");
            }
        }

        public static void WriteLong(BinaryWriter writer, long length, long value) {
            switch (length) {
                case 1:
                    writer.Write((byte) value);
                    break;
                case 2:
                    writer.Write((ushort) value);
                    break;
                case 4:
                    writer.Write((int) value);
                    break;
                case 8:
                    writer.Write(value);
                    break;
                default: throw new Exception($"Unable to write with length:{length}");
            }
        }

        public static void WriteFloat(BinaryWriter writer, long length, float value) {
            switch (length) {
                case 4:
                    writer.Write(value);
                    break;
                case 8:
                    writer.Write((double) value);
                    break;
                default: throw new Exception($"Unable to write with length:{length}");
            }
        }

        public static void WriteBool(BinaryWriter writer, long length, bool value) {
            WriteInt(writer, length, value ? 1 : 0);
        }


        public static unsafe float FastReadFloat(byte[] buffer, long offset) {
            uint val = buffer[offset + 0] |
                       (uint) buffer[offset + 1] << 8 |
                       (uint) buffer[offset + 2] << 16 |
                       (uint) buffer[offset + 3] << 24;
            return *((float*) &val);
        }

        public static void FastWriteFloat(byte[] buffer, float value, long offset) {
            var bs = BitConverter.GetBytes(value);
            buffer[offset + 0] = bs[0];
            buffer[offset + 1] = bs[1];
            buffer[offset + 2] = bs[2];
            buffer[offset + 3] = bs[3];
        }


        public static Vector3 FastReadVec3(byte[] buffer, long offset) {
            return new Vector3(
                FastReadFloat(buffer, offset + 0),
                -FastReadFloat(buffer, offset + 4),
                FastReadFloat(buffer, offset + 8));
        }

        public static void FastReadVec3(ref Vector3 dest, byte[] buffer, long offset) {
            dest.x = FastReadFloat(buffer, offset + 0);
            dest.y = -FastReadFloat(buffer, offset + 4);
            dest.z = FastReadFloat(buffer, offset + 8);
        }

        public static void ReadColour32(ref Color32 dest, byte[] buffer, long offset) {
            dest.a = buffer[offset + 3];
            dest.r = buffer[offset + 2];
            dest.g = buffer[offset + 1];
            dest.b = buffer[offset + 0];
        }
        
        public static void ReadColour32(ref Color32 dest, BinaryReader reader, long length)
        {
            if( length != 4) throw new Exception("Color32 instantiable from 4 bytes");
            ReadColour32(ref dest, reader.ReadBytes(4), 0);
           
        }

        public static void FastWriteVec3(Vector3 source, byte[] buffer, long offset) {
            FastWriteFloat(buffer, source.x, offset + 0);
            FastWriteFloat(buffer, source.y, offset + 4);
            FastWriteFloat(buffer, source.z, offset + 8);
        }

        public static void WriteVec3(BinaryWriter writer, long length, Vector3 v) {
            if (length != 4)
                throw new Exception($"float with length {length} is not supported");
            writer.Write(v.x);
            writer.Write(v.y);
            writer.Write(v.z);
        }
        
        public static void ReadVec3(ref Vector3 vec, BinaryReader reader, long length) {
            if (length != 4)
                throw new Exception($"float with length {length} is not supported");
            vec.x = reader.ReadSingle();
            vec.y = reader.ReadSingle();
            vec.z = reader.ReadSingle();
        }
        
    }
}