using static libfluid.Mmf;

namespace libfluid {
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using System.IO;
    using System.Threading.Tasks;

    [Serializable]
    public class Entry {
        public string name;
        public long size;

        public override string ToString() {
            return $"Entry({name}={size})";
        }
    }

    [Serializable]
    public class StructDef {
        private int size;
        public List<Entry> fields;

        public override string ToString() {
            return "Entries[" + size + "](" + string.Join(",", fields) + ")";
        }


        public long Resolve(string name, int index) {
            var found = fields.Select((entry, i) => new {entry, i})
                .FirstOrDefault(x => x.entry.name == name && x.i == index);
            if (found == null)
                throw new Exception($"Unable to resolve {name} at [{index}]");
            return found.entry.size;
        }
    }

    public class Defs {
        public StructDef vec3;
        public StructDef meshTriangle;
        public StructDef header;
        public StructDef sceneMeta;
        public StructDef query;
        public StructDef queryResult;
        public StructDef well;
        public StructDef source;
        public StructDef drain;

        public static Defs Read(string path) {
            return JsonUtility.FromJson<Defs>(File.ReadAllText(path));
        }
    }


    public struct Header {
        public long timestamp;
        public int entries;
        public int written;

        public override string ToString() {
            return
                $"Header({nameof(timestamp)}: {timestamp}, {nameof(entries)}: {entries}, {nameof(written)}: {written})";
        }

        public Header(int size) :
            this(DateTimeOffset.Now.ToUnixTimeMilliseconds(), size, size) { }

        public Header(long timestamp, int entries, int written) {
            this.timestamp = timestamp;
            this.entries = entries;
            this.written = written;
        }

        public static Header Read(BinaryReader reader, StructDef def) {
            var timestamp = def.Resolve("timestamp", 0);
            var entries = def.Resolve("entries", 1);
            var written = def.Resolve("written", 2);
            return new Header(
                ReadLong(reader, timestamp),
                ReadInt(reader, entries),
                ReadInt(reader, written)
            );
        }

        public static Action<BinaryWriter, Header> Writer(StructDef headerDef) {
            var timestamp = headerDef.Resolve("timestamp", 0);
            var entries = headerDef.Resolve("entries", 1);
            var written = headerDef.Resolve("written", 2);
            return (writer, header) => {
                WriteLong(writer, timestamp, header.timestamp);
                WriteInt(writer, entries, header.entries);
                WriteInt(writer, written, header.written);
            };
        }
    }

    public class Well {
        public Vector3 centre;
        public float force;

        public Well(Vector3 centre, float force) {
            this.centre = centre;
            this.force = force;
        }

        public static Action<BinaryWriter, IList<Well>> Writer(
            StructDef wellDef, Action<BinaryWriter, Header> headerWriter) {
            var wellx = wellDef.Resolve("well.x", 0);
            var welly = wellDef.Resolve("well.y", 1);
            var wellz = wellDef.Resolve("well.z", 2);
            var force = wellDef.Resolve("force", 3);
            return (writer, wells) => {
                headerWriter(writer, new Header(wells.Count));
                foreach (var well in wells) {
                    WriteVec3(writer, wellx, well.centre);
                    WriteFloat(writer, force, well.force);
                }
            };
        }
    }

    public class Source {
        public Vector3 centre;
        public Vector3 velocity;
        public long rate;
        public long tag;
        public Color32 colour;

        public Source(Vector3 centre, Vector3 velocity, long rate, long tag, Color32 colour) {
            this.centre = centre;
            this.velocity = velocity;
            this.rate = rate;
            this.tag = tag;
            this.colour = colour;
        }

        public static Action<BinaryWriter, IList<Source>> Writer(
            StructDef sourceDef, Action<BinaryWriter, Header> headerWriter) {
            var sourcex = sourceDef.Resolve("source.x", 0);
            var sourcey = sourceDef.Resolve("source.y", 1);
            var sourcez = sourceDef.Resolve("source.z", 2);
            var source_velx = sourceDef.Resolve("source.vel.x", 3);
            var source_vely = sourceDef.Resolve("source.vel.y", 4);
            var source_velz = sourceDef.Resolve("source.vel.z", 5);
            var rate = sourceDef.Resolve("rate", 6);
            var tag = sourceDef.Resolve("tag", 7);
            var colour = sourceDef.Resolve("colour", 8);
            return (writer, sources) => {
                headerWriter(writer, new Header(sources.Count));
                foreach (var source in sources) {
                    WriteVec3(writer, sourcex, source.centre);
                    WriteVec3(writer, source_velx, source.velocity);
                    WriteLong(writer, rate, source.rate);
                    WriteLong(writer, tag, source.tag);
                    WriteInt(writer, colour, ((source.colour.a & 0xFF) << 24) |
                                             ((source.colour.r & 0xFF) << 16) |
                                             ((source.colour.g & 0xFF) << 8) |
                                             ((source.colour.b & 0xFF) << 0));
                }
            };
        }
    }

    public class Drain {
        public Vector3 centre;
        public float width;
        public float depth;

        public Drain(Vector3 centre, float width, float depth) {
            this.centre = centre;
            this.width = width;
            this.depth = depth;
        }

        public static Action<BinaryWriter, IList<Drain>> Writer(
            StructDef drainDef, Action<BinaryWriter, Header> headerWriter) {
            var drainx = drainDef.Resolve("drain.x", 0);
            var drainy = drainDef.Resolve("drain.y", 1);
            var drainz = drainDef.Resolve("drain.z", 2);
            var width = drainDef.Resolve("width", 3);
            var depth = drainDef.Resolve("depth", 4);
            return (writer, drains) => {
                headerWriter(writer, new Header(drains.Count));
                foreach (var drain in drains) {
                    WriteVec3(writer, drainx, drain.centre);
                    WriteFloat(writer, width, drain.width);
                    WriteFloat(writer, depth, drain.depth);
                }
            };
        }
    }

    public class SceneMeta {
        public bool suspend = false;
        public bool terminate = false;
        public int solverIter = 3;
        public float solverStep = 1;
        public float solverScale = 1000;
        public float surfaceRes = 1;
        public float gravity = 9.8f;
        public Vector3 minBound = new Vector3();
        public Vector3 maxBound = new Vector3(10, 10, 10);
        public SceneMeta() { }
        
        
        
    }

    public class Scene {
        public SceneMeta meta = new SceneMeta();
        public IList<Well> wells;
        public IList<Source> sources;
        public IList<Drain> drains;
        public IList<Query> queries;

        public static Action<BinaryWriter, Scene> MakeWriter(
            StructDef metaDef,
            Action<BinaryWriter, IList<Well>> wellWriter,
            Action<BinaryWriter, IList<Source>> sourceWriter,
            Action<BinaryWriter, IList<Drain>> drainWriter,
            Action<BinaryWriter, IList<Query>> queryWriter
        ) {
            var suspend = metaDef.Resolve("suspend", 0);
            var terminate = metaDef.Resolve("terminate", 1);
            var solverIter = metaDef.Resolve("solverIter", 2);
            var solverStep = metaDef.Resolve("solverStep", 3);
            var solverScale = metaDef.Resolve("solverScale", 4);
            var surfaceRes = metaDef.Resolve("surfaceRes", 5);
            var gravity = metaDef.Resolve("gravity", 6);

            var minBoundx = metaDef.Resolve("minBound.x", 7);
            var minBoundy = metaDef.Resolve("minBound.y", 8);
            var minBoundz = metaDef.Resolve("minBound.z", 9);

            var maxBoundx = metaDef.Resolve("maxBound.x", 10);
            var maxBoundy = metaDef.Resolve("maxBound.y", 11);
            var maxBoundz = metaDef.Resolve("maxBound.z", 12);

            return (writer, scene) => {
                WriteBool(writer, suspend, scene.meta.suspend);
                WriteBool(writer, terminate, scene.meta.terminate);
                WriteInt(writer, solverIter, scene.meta.solverIter);
                WriteFloat(writer, solverStep, scene.meta.solverStep);
                WriteFloat(writer, solverScale, scene.meta.solverScale);
                WriteFloat(writer, surfaceRes, scene.meta.surfaceRes);
                WriteFloat(writer, gravity, scene.meta.gravity);
                WriteVec3(writer, minBoundx, scene.meta.minBound);
                WriteVec3(writer, maxBoundx, scene.meta.maxBound);
                wellWriter(writer, scene.wells);
                sourceWriter(writer, scene.sources);
                drainWriter(writer, scene.drains);
                queryWriter(writer, scene.queries);
            };
        }
    }


    public class Query {
        public long id;
        public Vector3 position;

        public Query(long id, Vector3 position) {
            this.id = id;
            this.position = position;
        }

        public override string ToString() {
            return $"Query{nameof(id)}: {id}, {nameof(position)}: {position}";
        }

        public static Action<BinaryWriter, IList<Query>> MakeWriter(
            StructDef queryDef,
            Action<BinaryWriter, Header> headerWriter
        ) {
            var id = queryDef.Resolve("query.id", 0);
            var qx = queryDef.Resolve("query.x", 1);
            var qy = queryDef.Resolve("query.y", 2);
            var qz = queryDef.Resolve("query.z", 3);
            return (writer, queries) => {
                headerWriter(writer, new Header(queries.Count));
                foreach (var query in queries) {
                    WriteLong(writer, id, query.id);
                    WriteVec3(writer, qx, query.position);
                }
            };
        }
    }


    public struct QueryResult {
        public long id;
        public Vector3 position;
        public int neighbours;
        public Color32 averageColour;

        public override string ToString() {
            return
                $"QueryResult{nameof(position)}: {position}, {nameof(neighbours)}: {neighbours}, {nameof(averageColour)}: {averageColour}";
        }

        public static Action<float, FluidQuery[], BinaryReader> MakeReader(
            StructDef headerDef,
            StructDef queryResultDef) {
            var id = queryResultDef.Resolve("query.id", 0);
            var qx = queryResultDef.Resolve("query.x", 1);
            var qy = queryResultDef.Resolve("query.y", 2);
            var qz = queryResultDef.Resolve("query.z", 3);
            var neighbours = queryResultDef.Resolve("neighbours", 4);
            var avgcolour = queryResultDef.Resolve("avgcolour", 5);

            return (scale, queries, reader) => {
                var header = Header.Read(reader, headerDef);
//                Debug.Log(">>"+header);
                QueryResult[] xs = new QueryResult[header.entries];
                for (int i = 0; i < header.entries; i++) {
                    xs[i].id = ReadLong(reader, id);
                    ReadVec3(ref xs[i].position, reader, qx);
                    xs[i].neighbours = ReadInt(reader, neighbours);
                    ReadColour32(ref xs[i].averageColour, reader, avgcolour);
                }

                IDictionary<long, QueryResult> results = xs.ToDictionary(x => x.id, x => x);
                foreach (var query in queries) {
                    query.ClearQuery();
                    if (results.ContainsKey(query.Id())) {
                        query.UpdateQuery(results[query.Id()]);
                    }
                }
            };
        }
    }


    public class RigidBodyCollider {
        public static Action<BinaryWriter, Vector3[]> MakeWriter(
            Action<BinaryWriter, Header> headerWriter,
            StructDef v3fDef) {
            var v3x = v3fDef.Resolve("vec3.x", 0);
            var v3y = v3fDef.Resolve("vec3.y", 1);
            var v3z = v3fDef.Resolve("vec3.z", 2);

            PinnedMonotonicArray<float> buffer = new PinnedMonotonicArray<float>(1024);
            PinnedMonotonicArray<byte> byteBuffer = new PinnedMonotonicArray<byte>(1024);

            return (writer, vectors) => {
                headerWriter(writer, new Header(vectors.Length));

                buffer.Ensure(vectors.Length * 3);
                byteBuffer.Ensure((int) (vectors.Length * 3 * v3x));
                for (int i = 0; i < vectors.Length; i++) {
                    buffer.Data[i * 3 + 0] = vectors[i].x;
                    buffer.Data[i * 3 + 1] = vectors[i].y;
                    buffer.Data[i * 3 + 2] = vectors[i].z;
                }

                Buffer.BlockCopy(buffer.Data, 0, byteBuffer.Data, 0, byteBuffer.Length);
                writer.BaseStream.Write(byteBuffer.Data, 0, byteBuffer.Length);
            };
        }
    }

    // Query ->

    public class TriangleMesh {
        public static Action<Mesh, BinaryReader> MakeReader(
            StructDef trigDef, StructDef headerDef) {
            var v0x = trigDef.Resolve("v0.x", 0);
            var v0y = trigDef.Resolve("v0.y", 1);
            var v0z = trigDef.Resolve("v0.z", 2);
            var v1x = trigDef.Resolve("v1.x", 3);
            var v1y = trigDef.Resolve("v1.y", 4);
            var v1z = trigDef.Resolve("v1.z", 5);
            var v2x = trigDef.Resolve("v2.x", 6);
            var v2y = trigDef.Resolve("v2.y", 7);
            var v2z = trigDef.Resolve("v2.z", 8);

            var n0x = trigDef.Resolve("n0.x", 9);
            var n0y = trigDef.Resolve("n0.y", 10);
            var n0z = trigDef.Resolve("n0.z", 11);
            var n1x = trigDef.Resolve("n1.x", 12);
            var n1y = trigDef.Resolve("n1.y", 13);
            var n1z = trigDef.Resolve("n1.z", 14);
            var n2x = trigDef.Resolve("n2.x", 15);
            var n2y = trigDef.Resolve("n2.y", 16);
            var n2z = trigDef.Resolve("n2.z", 17);

            var cx = trigDef.Resolve("c.x", 18);
            var cy = trigDef.Resolve("c.y", 19);
            var cz = trigDef.Resolve("c.z", 20);

            if (new HashSet<long> {
                v0x, v0y, v0z, v1x, v1y, v1z, v2x, v2y, v2z,
                n0x, n0y, n0z, n1x, n1y, n1z, n2x, n2y, n2z
            }.Count != 1) throw new Exception("Staging with unequal is not supported");


            if (new HashSet<long> {cx, cy, cz}.Count != 1)
                throw new Exception("Staging with unequal is not supported");


            var floatSize = v0x;
            var intSize = cx;
            long lastUpdated = 0;


            PinnedMonotonicArray<byte> buffer = new PinnedMonotonicArray<byte>(4096);

            PinnedMonotonicArray<Vector3> vertices = new PinnedMonotonicArray<Vector3>(4096);
            PinnedMonotonicArray<Vector3> normals = new PinnedMonotonicArray<Vector3>(4096);
            PinnedMonotonicArray<Color32> colours = new PinnedMonotonicArray<Color32>(4096);
            PinnedMonotonicArray<int> triangles = new PinnedMonotonicArray<int>(4096);

            return (mesh, reader) => {
                reader.BaseStream.Seek(0, SeekOrigin.Begin);

                var header = Header.Read(reader, headerDef);

                if (header.timestamp == lastUpdated ||
                    header.entries != header.written) return;


                if (header.entries == 0) {
                    mesh.Clear();
                    return;
                }

                lastUpdated = header.timestamp;
                int pointCount = header.entries * 3;
                long start = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                // entries * 3 * 3 * 2 =
                // entries *
                // vertex per triangle *
                // float per 3d coord *
                // (normal + vertex, i.e. 1+1)

                long vertexFloatSize = floatSize * pointCount * 3;
                long normalFloatSize = floatSize * pointCount * 3;
                long colourIntSize = intSize * pointCount;

                buffer.Ensure((int) (vertexFloatSize + normalFloatSize + colourIntSize));
                // XXX starts from the current offset so offset is 0
                reader.BaseStream.Read(buffer.Data, 0, buffer.Length);

                var vecSize = floatSize * 3;
                var begin = pointCount - 1;

                triangles.Ensure(pointCount);
                vertices.Ensure(pointCount);
                normals.Ensure(pointCount);
                colours.Ensure(pointCount);

                Parallel.ForEach(Partitioner.Create(0, header.entries),
                    (range, ls) => {
                        for (int i = range.Item1; i < range.Item2; i++) {
                            var offset = 3 * i;
                            var x = offset + 0;
                            var y = offset + 1;
                            var z = offset + 2;
                            triangles.Data[x] = x;
                            triangles.Data[y] = y;
                            triangles.Data[z] = z;
                            var mem = buffer.Data;

                            long offsetVN = (3 + 3) * i * vecSize;
                            long offsetC = (3) * i * intSize;

                            FastReadVec3(ref vertices.Data[begin - x], mem,
                                offsetVN + 0 * vecSize + offsetC);
                            FastReadVec3(ref vertices.Data[begin - y], mem,
                                offsetVN + 1 * vecSize + offsetC);
                            FastReadVec3(ref vertices.Data[begin - z], mem,
                                offsetVN + 2 * vecSize + offsetC);
                            FastReadVec3(ref normals.Data[begin - x], mem,
                                offsetVN + 3 * vecSize + offsetC);
                            FastReadVec3(ref normals.Data[begin - y], mem,
                                offsetVN + 4 * vecSize + offsetC);
                            FastReadVec3(ref normals.Data[begin - z], mem,
                                offsetVN + 5 * vecSize + offsetC);
                            ReadColour32(ref colours.Data[begin - x], mem,
                                offsetVN + 6 * vecSize + offsetC + 0 * intSize);
                            ReadColour32(ref colours.Data[begin - y], mem,
                                offsetVN + 6 * vecSize + offsetC + 1 * intSize);
                            ReadColour32(ref colours.Data[begin - z], mem,
                                offsetVN + 6 * vecSize + offsetC + 2 * intSize);
                        }
                    });

//                    if (triangles.Data.Length > triangles.Length) {
//                        for (int i =  triangles.Length; i < triangles.Data.Length; i++) {
//                            triangles.Data[i] = 0;
//                        }
//                    }

                long end = DateTimeOffset.Now.ToUnixTimeMilliseconds();
//                Debug.Log(
//                    $"Read triangles: {(end - start)}ms @ {header.entries} VN={pointCount} (time={lastUpdated})");
                mesh.MarkDynamic();
                mesh.Clear();
                mesh.vertices = vertices.Data;
                mesh.normals = normals.Data;
                mesh.colors32 = colours.Data;
                mesh.triangles = triangles.Compacted();
            };
        }
    }
}