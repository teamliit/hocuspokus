using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace libfluid {
    public class FluidCollider : MonoBehaviour {
        public bool dynamic = true;
        private IList<Vector3> samples;

        public float spacing = 1f;

        // XXX Unity overrides == and implements custom null, this is a whole new level of crazy
        private static void CaptureAndUndoIfNotNull<T, P>(T t,
            Func<T, P> get,
            Action<T> mutate, Action f,
            Action<T, P> set) where T : UnityEngine.Object {
            if (t == null) f();
            else {
                var p = get(t);
                mutate(t);
                f();
                set(t, p);
            }
        }

        void Start() {
            var target = GetComponent<Collider>();
            if (target == null) throw new Exception("FluidCollider needs a Collider of some sort");


            CaptureAndUndoIfNotNull(
                GetComponent<Rigidbody>(),
                c => c.isKinematic,
                c => c.isKinematic = true,
                () => {
                    CaptureAndUndoIfNotNull(
                        target as MeshCollider,
                        c => c.convex,
                        c => c.convex = false,
                        () => samples = CastUtils.Sample6D(target, spacing * 0.1f),
                        (c, convex) => c.convex = convex
                    );
                },
                (c, kinematic) => c.isKinematic = kinematic
            );

            Debug.Log($"{name} generated {samples.Count} samples");

            if (samples == null) {
                throw new Exception("No samples were generated");
            }

            transform.hasChanged = true;
            //sShowSamplePoints();
        }

        public int RigidBodyPointCount() {
            return enabled ? samples.Count : 0;
        }

        public bool QueryRigidBodyPoints(int offset, Vector3[] buffer, float scale) {
            if (!enabled) return false;
            if (transform.hasChanged || dynamic) {
                var mat = transform.localToWorldMatrix;
//                Debug.Log($"Handle @{offset} N={samples.Count}");
                if (samples.Count != 0) {
                    if (samples.Count < 512) {
                        for (int i = 0; i < samples.Count; i++) {
                            buffer[offset + i] =
                                FluidMesh.yFlipped(mat.MultiplyPoint3x4(samples[i])) * scale;
                        }
                    }
                    else {
                        Parallel.ForEach(Partitioner.Create(0, samples.Count),
                            (range, ls) => {
                                for (int i = range.Item1; i < range.Item2; i++) {
                                    buffer[offset + i] =
                                        FluidMesh.yFlipped(mat.MultiplyPoint3x4(samples[i])) *
                                        scale;
                                }
                            });
                    }
                }

//                for (var i = 0; i < samples.Count; i++) {
//                    buffer[offset + i] =
//                        FluidMesh.yFlipped(transform.TransformPoint(samples[i])) * scale;
//                }

                return true;
            }

            return false;
        }


//        public Vector3[] QueryRigidBodyPoints() {
//            long start = DateTimeOffset.Now.ToUnixTimeMilliseconds();
//            if (transform.hasChanged) {
//                for (var i = 0; i < samples.Count; i++) {
//                    buffer[i] = transform.TransformPoint(samples[i]);
//                }
//            }
//            long end = DateTimeOffset.Now.ToUnixTimeMilliseconds();
//            Debug.Log(
//                $"Query in {end - start}ms for N={samples.Count}, changed={transform.hasChanged}");
//            return buffer;
//        }

        private void ShowSamplePoints() {
            Vector3[] buffer = new Vector3[RigidBodyPointCount()];
            QueryRigidBodyPoints(0, buffer, 1);
            foreach (var p in buffer) {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = FluidMesh.yFlipped(p);
                sphere.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                sphere.GetComponent<Renderer>().material.color = Color.red;
                sphere.GetComponent<Collider>().enabled = false;
            }
        }

        void Update() {
            transform.hasChanged = false;
        }
    }
}