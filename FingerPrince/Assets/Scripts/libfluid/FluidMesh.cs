using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.IO.MemoryMappedFiles;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Valve.VR.InteractionSystem;

namespace libfluid {
    public class FluidMesh : MonoBehaviour {
        [Range(1, 100)] public int solverIteration = 3;
        [Range(0.1f, 10)] public float solverStep = 1;
        [Range(0.1f, 150f)] public float solverScale = 50f;
        [Range(0.10f, 8f)] public float surfaceResolution = 1;
        [Range(-20, 20)] public float gravity = 9.8f;
        public Material material;
        public float FluidScale = 10f;


        // FIXME needs staging
        private void UpdateParticles(float scale, Vector3 offset) {
            using (var reader = new BinaryReader(particlesMmf.CreateViewStream())) {
                // TODO read json and decide size
                // long is 4 bytes on Windows and 8 on Linux
                long timestamp = reader.ReadInt64();
                long length = reader.ReadInt64();
                if (timestamp != particleUpdated) {
                    particleUpdated = timestamp;
                    // Debug.Log($">>  {timestamp} {length}...");
                    for (int i = 0; i < length; i++) {
                        long t = reader.ReadInt64();
                        int tpe = reader.ReadInt32();
                        float mass = reader.ReadSingle();
                        var pos = new Vector3(reader.ReadSingle(), -reader.ReadSingle(),
                                      reader.ReadSingle()) * scale + offset;
                        var vel = new Vector3(reader.ReadSingle(), reader.ReadSingle(),
                            reader.ReadSingle());

                        if (!particles.ContainsKey(t)) {
                            particles[t] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            particles[t].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                            particles[t].GetComponent<Renderer>().material =
                                GetComponent<Renderer>().material;
                        }

                        particles[t].transform.position = pos;
                        // Debug.Log($">> \t t={t} tpe={tpe} m={mass} p={pos} v={vel}");
                    }
                }
            }
        }

        public string dataPath = @"/home/tom/libfluid/cmake-build-release/samples";
//    public string dataPath = @"C:\Users\Tom\libfluid\build\Release\samples";

        private MemoryMappedFile particlesMmf;
        private long particleUpdated = 0;
        private long surfaceUpdated = 0;

        private MmfIO surfaceIO;
        private MmfIO sceneIO;
        private MmfIO queryIO;

        private MmfIO dynamicColliderIO;
//        private MmfIO staticColliderIO;

        private Dictionary<long, GameObject> particles = new Dictionary<long, GameObject>();

        private Action<Mesh, BinaryReader> surfaceReader;
        private Action<BinaryWriter, Scene> sceneWriter;
        private Action<float, FluidQuery[], BinaryReader> queryReader;
        private Action<BinaryWriter, Vector3[]> colliderWriter;

        private Tuple<GameObject, Mesh> meshNode;


        private static Tuple<GameObject, Mesh> MakeFluidNode(float scale, Material material) {
            var node = new GameObject("fluidMesh", typeof(MeshFilter), typeof(MeshRenderer));
            node.GetComponent<Renderer>().material = material;
            node.transform.localScale = new Vector3(
                1f / scale,
                1f / scale,
                1f / scale);
            var mesh = node.GetComponent<MeshFilter>().mesh;
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            return Tuple.Create(node, mesh);
        }

        private void OnValidate() {
            // FIXME NPE
            //meshNode.Item1.GetComponent<Renderer>().material = material;
        }

        void Start() {
            Debug.Log("Reading MMF");

            meshNode = MakeFluidNode(FluidScale, material);

            var defs = Defs.Read(Path.Combine(dataPath, "defs.json"));
            var headerWriter = Header.Writer(defs.header);


            var scenePath = Path.Combine(dataPath, "scene.mmf");
            var queryPath = Path.Combine(dataPath, "query.mmf");
            // XXX do not delete! MMF would read from the wrong (deleted) region
//            File.Delete(scenePath);
//            File.Delete(queryPath);
            sceneIO = new MmfIO(scenePath, 1024 * 1024 * 2);
            queryIO = new MmfIO(queryPath, 1024 * 1024 * 1);

            surfaceIO = new MmfIO(Path.Combine(dataPath, "triangles.mmf"), 1024 * 1024 * 128);

//            staticColliderIO = new MmfIO(Path.Combine(dataPath, "static_bodies.mmf"));
            dynamicColliderIO = new MmfIO(Path.Combine(dataPath, "dynamic_bodies.mmf"),
                1024 * 1024 * 256);

            sceneWriter = Scene.MakeWriter(
                defs.sceneMeta,
                Well.Writer(defs.well, headerWriter),
                Source.Writer(defs.source, headerWriter),
                Drain.Writer(defs.drain, headerWriter),
                Query.MakeWriter(defs.query, headerWriter)
            );
            surfaceReader = TriangleMesh.MakeReader(defs.meshTriangle, defs.header);

            queryReader = QueryResult.MakeReader(defs.header, defs.queryResult);

            colliderWriter = RigidBodyCollider.MakeWriter(headerWriter, defs.vec3);
        }

        private Scene scene = new Scene();

        public static void flipY(Vector3 v) {
            v.y = -v.y;
        }

        public static Vector3 yFlipped(Vector3 v) {
            return new Vector3(v.x, -v.y, v.z);
        }

        private Vector3[] staticColliderBuffer;
        private Vector3[] dynamicColliderBuffer;


        private void CollectAndWriteCollider(IEnumerable<FluidCollider> cs,
            ref Vector3[] buffer,
            BinaryWriter writer) {
            long start = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var xs = cs as FluidCollider[] ?? cs.ToArray();
            if (buffer == null) {
                var nPoints = xs.Select(x => x.RigidBodyPointCount()).Sum();
                buffer = new Vector3[nPoints];
            }

            int offset = 0;
            foreach (var x in xs) {
                x.QueryRigidBodyPoints(offset, buffer, FluidScale);
                offset += x.RigidBodyPointCount();
            }

            long end = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            colliderWriter(writer, buffer);
            long end2 = DateTimeOffset.Now.ToUnixTimeMilliseconds();
//            Debug.Log($"Collider: mat: {end - start}ms mmf:{end2 - end}ms ps={offset}");
        }


        void Update() {
            var xform = transform;
            var scale = xform.lossyScale;

            scene.meta.solverIter = solverIteration;
            scene.meta.solverScale =
                (solverScale / 100f) * FluidScale * scale.magnitude;
            scene.meta.solverStep = solverStep;
            scene.meta.surfaceRes = surfaceResolution;
            scene.meta.gravity = gravity;
            var origin = yFlipped(xform.position);
            var extent = (scale) / 2;

            scene.meta.minBound = (origin - extent) * FluidScale;
            scene.meta.maxBound = (origin + extent) * FluidScale;
            //Debug.Log("->" + scene.meta.minBound + " "+ scene.meta.maxBound + " " + (scale * FluidScale));


//            Debug.Log(scene.meta.minBound + " " + scene.meta.maxBound);

            scene.wells = FindObjectsOfType<FluidWell>().Select(x => x.Data(FluidScale)).ToArray();
            scene.sources = FindObjectsOfType<FluidSource>().Select(x => x.Data(FluidScale))
                .ToArray();
            scene.drains = FindObjectsOfType<FluidDrain>().Select(x => x.Data(FluidScale))
                .ToArray();
            scene.queries = FindObjectsOfType<FluidQuery>().Select(x => x.Data(FluidScale))
                .ToArray();

            scene.meta.suspend = true;
            Mmf.FlushAndReset(sceneIO.Writer);
            sceneWriter(sceneIO.Writer, scene);

            try {
                surfaceReader(meshNode.Item2, surfaceIO.Reader);
                queryReader(FluidScale, FindObjectsOfType<FluidQuery>(), queryIO.Reader);
            }
            finally {
                Mmf.FlushAndReset(queryIO.Reader);
                scene.meta.suspend = false;
                Mmf.FlushAndReset(sceneIO.Writer);
                sceneWriter(sceneIO.Writer, scene);
            }
        }


        private void HandleCollider() {
            var colliders = FindObjectsOfType<FluidCollider>();

            CollectAndWriteCollider(colliders.Where(c => c.enabled),
                ref dynamicColliderBuffer, dynamicColliderIO.Writer);
        }


        private long last = 0;

        private void FixedUpdate() {
            var now = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            if (now - last < 32) return;
            last = now;

            try {
                HandleCollider();
            }
            finally {
                Mmf.FlushAndReset(dynamicColliderIO.Writer);
            }

            //            CollectAndWriteCollider(colliders.Where(c => !c.dynamic && c.enabled),
//                ref staticColliderBuffer, staticColliderIO.Writer);
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.blue;
            var xform = transform;

            Gizmos.DrawWireSphere(xform.position,
                (solverScale / 100f) * xform.lossyScale.magnitude / 2);
            Gizmos.DrawWireCube(xform.position, xform.lossyScale);
        }

        void OnDestroy() {
            surfaceIO.Dispose();
            sceneIO.Dispose();
            queryIO.Dispose();
            dynamicColliderIO.Dispose();
//            staticColliderIO.Dispose();
            Debug.Log("All MMF IO released");
        }
    }
}