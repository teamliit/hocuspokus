using System;
using System.Collections.Generic;
using System.Linq;
using libfluid;
using Optional;
using UnityEngine;


namespace libfluid {
    public class FluidAggregateQuery : MonoBehaviour {
        public Option<Color> Colour { get; private set; }
        public Option<int> Neighbours { get; private set; }

        private IList<FluidQuery> queries = new List<FluidQuery>();


        private static Vector3[] Points = {
            Vector3.zero,
            Vector3.up * 0.5f,
            Vector3.down * 0.5f,
            Vector3.left * 0.5f,
            Vector3.right * 0.5f,
            Vector3.back * 0.5f,
            Vector3.forward * 0.5f
        };

        private void Start() {
            foreach (var p in Points) queries.Add(FluidQuery.AffixTo(gameObject, p));
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.white;
            foreach (var p in Points) Gizmos.DrawWireSphere(transform.TransformPoint(p), 0.2f);
        }

        static Option<T> FoldLeft<T>(IEnumerable<T> xs, Func<T, T, T> f) {
            return xs.Aggregate(Option.None<T>(), (acc, y) => acc.Map(x => f(x, y)).Or(y));
        }

        static Option<T> Average<T>(IEnumerable<T> xs, Func<T, T, T> f, Func<T, int, T> avg) {
            var ts = xs.ToList();
            return FoldLeft(ts, f).Map(x => avg(x, ts.Count));
        }


        private Option<Color> FastAverageColour() {
            int N = 0;
            float r = 0, g = 0, b = 0, a = 0;
            foreach (var query in queries) {
                query.AverageColour().MatchSome(x => {
                    r += x.r;
                    g += x.g;
                    b += x.b;
                    a += x.a;
                    N++;
                });
            }

            return N == 0
                ? Option.None<Color>()
                : new Color(r / 255 / N, g / 255 / N, b / 255 / N, a / 255 / N).Some();
        }

        private Option<int> FastNeighbours() {
            int N = 0;
            float sigma = 0;
            foreach (var query in queries) {
                query.Neighbours().MatchSome(x => {
                    sigma += x;
                    N++;
                });
            }

            return N == 0 ? Option.None<int>() : Mathf.RoundToInt(sigma / N).Some();
        }


        void Update() {
            // C# is garbage, Scala equv:
            // (queries >>= Neighbours) map (new Vector3(_,_,_,_)) reduceLeft(x + y) map (_ / sigma)  


            Colour = FastAverageColour();
            Neighbours = FastNeighbours();

//            Colour = Average(queries
//                        .SelectMany(x => x.AverageColour().ToEnumerable())
//                        .Select(x => new Vector4(x.r, x.g, x.b, x.a)),
//                    (x, y) => x + y,
//                    (sigma, n) => (sigma / n) / 255)
//                .Map(v => new Color(v.x, v.y, v.z, v.w));
//
//
//            Neighbours = Average(queries
//                    .SelectMany(x => x.Neighbours().ToEnumerable()),
//                (x, y) => x + y,
//                (sigma, n) => sigma / n);


//            Debug.Log(Colour + " ->  " + Neighbours + " <- " +
//                      queries.SelectMany(x => x.AverageColour().ToEnumerable()).Count());
        }
    }
}