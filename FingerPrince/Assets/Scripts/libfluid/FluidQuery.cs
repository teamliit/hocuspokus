using System.Threading;
using Optional;
using UnityEngine;

namespace libfluid {
    public class FluidQuery : MonoBehaviour {
        private static long AtomicCouter = 0;

        private static long MakeId() {
            return Interlocked.Increment(ref AtomicCouter);
        }

        public static FluidQuery AffixTo(GameObject that, Vector3 offset) {
            var query = that.AddComponent<FluidQuery>();
            query.localOffset = offset;
            return query;
        }

        public Vector3 localOffset = Vector3.zero;
        private long id;


        private void Start() {
            id = MakeId();
        }

        public long Id() {
            return id;
        }

        private Option<QueryResult> query = Option.None<QueryResult>();

        public Query Data(float scale) {
            return new Query(id, FluidMesh.yFlipped(transform.TransformPoint(localOffset)) * scale);
        }

        public void ClearQuery() {
            query = Option.None<QueryResult>();
        }

        public void UpdateQuery(QueryResult query) {
            this.query = query.Some();
        }

        public Option<int> Neighbours() {
            return query.Map(x => x.neighbours);
        }

        public Option<Color32> AverageColour() {
            return query.Map(x => x.averageColour);
        }

        void OnDrawGizmos() {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(
                transform.TransformPoint(localOffset),
                0.2f);
        }
    }
}