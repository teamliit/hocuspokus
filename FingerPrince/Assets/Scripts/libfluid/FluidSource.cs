using System;
using UnityEngine;

namespace libfluid {
    public class FluidSource : MonoBehaviour {
        [Range(0, 100)] public long rate = 10;
        public long tag = 0;
        
        public Vector3 velocity = new Vector3(0, 9.8f, 0);
        public Color colour = Color.blue;

        public Source Data(float scale) {
            return new Source(FluidMesh.yFlipped(transform.position) * scale, velocity,  rate, tag, colour);
        }
        
        void OnDrawGizmos() {
            Gizmos.color = colour;
            Gizmos.DrawWireCube(transform.position, Vector3.one * rate);
        }
        
    }
}