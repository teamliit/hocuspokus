using libfluid;
using UnityEngine;


public class FluidIngredient : MonoBehaviour {
    
    private ClosedColour colour = ClosedColour.White;
    private FluidAggregateQuery query;
    
    private void Start() {
        query = gameObject.AddComponent<FluidAggregateQuery>();
    }

    private void Update() {

        query.Colour.MatchSome(c => {
            colour =  colour.Mix(ClosedColour.Snap(c));
            GetComponent<Renderer>().material.color = colour.AsColour();
        });

    }
}