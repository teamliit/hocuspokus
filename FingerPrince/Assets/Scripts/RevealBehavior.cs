﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevealBehavior : MonoBehaviour {

	public ParticleSystem effect;

	void OnDisable(){
		// doTransiton();
	}

	void OnEnable(){
		// doTransiton();
	}
	void Start(){
		doTransiton();
	}

	private void doTransiton(){
		Instantiate(effect, gameObject.transform.position, Quaternion.identity);
	}
	
}
