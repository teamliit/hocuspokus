﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuoyancyMeshBehaviour : MonoBehaviour
{

    public float density;
    public float viscosity;
    private List<Vector3> sub_surface_vertices = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider other) {

        var rb = other.GetComponent<Rigidbody>();
        if (rb == null) return;
        Vector3 buoyancy = calc_buoyancy(other);
        if (float.IsNaN(buoyancy.x) || float.IsNaN(buoyancy.y) || float.IsNaN(buoyancy.z)) buoyancy = new Vector3(0.0f, 0.0f, 0.0f);
        // Debug.Log("Buoyancy: " + buoyancy);
        Vector3 centre_of_drag = get_centre_of_drag();
        if (float.IsNaN(centre_of_drag.x) || float.IsNaN(centre_of_drag.y) || float.IsNaN(centre_of_drag.z)) centre_of_drag = new Vector3(0.0f, 0.0f, 0.0f);

        apply_drag(rb, other, centre_of_drag);

        other.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(buoyancy, centre_of_drag);
    }

    private Vector3 get_centre_of_drag(){
        Vector3 ssv_total = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 centre_of_drag = new Vector3(0.0f, 0.0f, 0.0f);

        if( sub_surface_vertices.Count > 0 ){
            foreach(Vector3 vertex in sub_surface_vertices){
                ssv_total += vertex;
            }
            centre_of_drag = ssv_total / (float) sub_surface_vertices.Count;
        }

        return centre_of_drag;
    }

    private void apply_drag(Rigidbody rigidbody, Collider other, Vector3 centre_of_drag){
        Vector3 velocity = rigidbody.velocity;
        Vector3 angular_velocity = rigidbody.angularVelocity;
        Vector3 position = rigidbody.position;

        Vector3 vsquared = new Vector3(Mathf.Pow(velocity.x, 2), Mathf.Pow(velocity.y, 2), Mathf.Pow(velocity.z, 2));
        if( velocity.x < 0 ) vsquared.x *= -1;
        if( velocity.y < 0 ) vsquared.x *= -1;
        if( velocity.z < 0 ) vsquared.x *= -1;
        Vector3 avsquared = new Vector3(Mathf.Pow(angular_velocity.x, 2), Mathf.Pow(angular_velocity.y, 2), Mathf.Pow(angular_velocity.z, 2));
        if( angular_velocity.x < 0 ) avsquared.x *= -1;
        if( angular_velocity.y < 0 ) avsquared.y *= -1;
        if( angular_velocity.z < 0 ) avsquared.z *= -1;
        Vector3 drag = 0.5f * vsquared * -1.0f * viscosity * 0.5f;
        Vector3 angular_drag = 0.5f * avsquared * -1.0f * viscosity * 0.5f;

        Vector3 extents = other.bounds.extents;
        Vector3 centre = other.bounds.center;
        float largest_dim = Mathf.Max(Mathf.Max(extents.x, extents.y), extents.z) * 1.1f;

        Vector3 point_infront = position + (velocity * largest_dim);
        Vector3 orthogonal_x = Vector3.Cross( (point_infront - position), velocity );
        Vector3 orthogonal_y = Vector3.Cross( velocity, orthogonal_x );
        RaycastHit hit;

        for (float x = -largest_dim; x <= largest_dim; x++){
            for (float y = -largest_dim; y <= largest_dim; y++){
                Vector3 start = point_infront + ( orthogonal_x * x ) + ( orthogonal_y * y );
                if ( Physics.Raycast(start, -velocity, out hit, 10)){
                    if(hit.collider == other) {

                        float distance = Vector3.Distance(hit.point, centre);
                        // add drag
                        rigidbody.AddForceAtPosition(drag/(2.0f*largest_dim), hit.point);

                        // add a torque = angular drag * distance from centre
                        if(other.GetType() == typeof(BoxCollider)) rigidbody.AddForceAtPosition(distance*angular_drag/(2.0f*largest_dim), hit.point);

                    }
                }
            }
        }

    }

    private Vector3 calc_buoyancy(Collider other){
        // calculate volume in different ways based on what sort of shape it is
        float volume = 0.0f;
        if( other.GetType() == typeof(BoxCollider) ) volume = get_partial_cube_volume(other);
        else if( other.GetType() == typeof(SphereCollider) ) volume = get_partial_sphere_volume(other);
        float displaced_mass = this.density * volume;
        Vector3 displaced_weight = displaced_mass * Physics.gravity;

        return -1.0f * displaced_weight;
    }

    private float get_partial_sphere_volume(Collider other){
        float volume = 0.0f;
        Collider liquid = this.gameObject.GetComponent<Collider>();
        SphereCollider sphere = other.gameObject.GetComponent<SphereCollider>();

        // to find local max_y do some raycasts down at the liquid from x,z coords of each corner of the object's bounding box
        Vector3 centre = other.bounds.center;
        List<Vector3> corners = new List<Vector3>();
        corners.Add(centre - new Vector3(other.bounds.size.x/2, 0.0f, 0.0f));
        corners.Add(centre + new Vector3(other.bounds.size.x/2, 0.0f, 0.0f));
        corners.Add(centre - new Vector3(0.0f, 0.0f, other.bounds.size.z/2));
        corners.Add(centre + new Vector3(0.0f, 0.0f, other.bounds.size.z/2));
        RaycastHit hit;
        float hit_count=0;
        float y_count=0;
        foreach( Vector3 corner in corners ){
            float above_liquid = liquid.bounds.center.y + liquid.bounds.size.y/2 + 0.1f;
            Vector3 start = new Vector3(corner.x, above_liquid, corner.z);
            Vector3 direction_down = new Vector3(0.0f, -1.0f, 0.0f);
            Ray ray = new Ray(start, direction_down);
            if ( liquid.Raycast(ray, out hit, 10) ){
                y_count += hit.point.y;
                hit_count++;
            }
        }
        float local_max_y = y_count / hit_count;

        float h;
        if( local_max_y < centre.y ) h = sphere.radius - (centre.y - local_max_y);
        else if( local_max_y > centre.y + sphere.radius ) return (4 * Mathf.PI * Mathf.Pow(sphere.radius,3)) / 3.0f;
        else h = sphere.radius + (local_max_y - centre.y);
        volume = ((Mathf.PI * h * h) / 3.0f)  * (3.0f * sphere.radius - h);

        return volume;
    }

    private float get_partial_cube_volume(Collider other){

        Collider liquid = this.gameObject.GetComponent<Collider>();
        if( sub_surface_vertices.Count > 0 ) sub_surface_vertices.Clear();

        // Transform transform = other.gameObject.GetComponent<Transform>();
        BoxCollider other_box = other.gameObject.GetComponent<BoxCollider>();
        Transform transform = other_box.transform;

        int queries = 5;
        Vector3 size = other_box.size;
        Vector3 centre = other_box.center;
        float min_x, max_x, min_y, max_y, min_z, max_z, x_step, y_step, z_step;

        min_x = centre.x - size.x/2;
        max_x = centre.x + size.x/2;
        x_step = (max_x-min_x)/queries;

        min_y = centre.y - size.y/2;
        max_y = centre.y + size.y/2;
        y_step = (max_y-min_y)/queries;

        min_z = centre.z - size.z/2;
        max_z = centre.z + size.z/2;
        z_step = (max_z-min_z)/queries;

        float total_queries = queries * queries * queries;
        float collision_count = 0;

        // to find local max_y do some raycasts down at the liquid from x,z coords of each corner of the object's bounding box
        List<Vector3> corners = new List<Vector3>();
        corners.Add(other.bounds.center - new Vector3(other.bounds.size.x/2, 0.0f, 0.0f));
        corners.Add(other.bounds.center + new Vector3(other.bounds.size.x/2, 0.0f, 0.0f));
        corners.Add(other.bounds.center - new Vector3(0.0f, 0.0f, other.bounds.size.z/2));
        corners.Add(other.bounds.center + new Vector3(0.0f, 0.0f, other.bounds.size.z/2));
        RaycastHit hit;
        float hit_count=0;
        float y_count=0;
        foreach( Vector3 corner in corners ){
            float above_liquid = liquid.bounds.center.y + liquid.bounds.size.y/2 + 0.1f;
            Vector3 start = new Vector3(corner.x, above_liquid, corner.z);
            Vector3 direction_down = new Vector3(0.0f, -1.0f, 0.0f);
            Ray ray = new Ray(start, direction_down);
            if ( liquid.Raycast(ray, out hit, 10) ){
                y_count += hit.point.y;
                hit_count++;
            }
        }
        float local_max_y = y_count / hit_count;

        // query lots of points within the bounds of the object, and see if the liquid contains them
        for(float x=min_x; x<=max_x; x+=x_step){
            for(float y=min_y; y<=max_y; y+=y_step){
                for(float z=min_z; z<=max_z; z+=z_step){
                    Vector3 point = new Vector3(x, y, z);
                    point = transform.TransformPoint(point);
                    if( liquid.bounds.Contains(point) && point.y < local_max_y ){
                        sub_surface_vertices.Add(point);
                        collision_count++;
                    }
                }
            }
        }

        float ratio = (collision_count/total_queries);
        float volume = ratio * get_total_mesh_volume(other);

        return volume;

    }

    private bool point_inside_collider (Vector3 point, BoxCollider collider )
     {
         point = collider.transform.InverseTransformPoint( point ) - collider.center;
         
         float halfX = (collider.size.x * 0.5f);
         float halfY = (collider.size.y * 0.5f);
         float halfZ = (collider.size.z * 0.5f);

         if( point.x < halfX && point.x > -halfX && 
             point.y < halfY && point.y > -halfY && 
             point.z < halfZ && point.z > -halfZ ) {
                 return true;
             }
         
         else return false;
     }

    private float get_total_mesh_volume(Collider other){
        Mesh mesh = other.gameObject.GetComponent<MeshFilter>().mesh;
        float volume = 0;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        for ( int i = 0; i < triangles.Length; i += 3){
            Vector3 v1 = vertices[triangles[i]];
            Vector3 v2 = vertices[triangles[i+1]];
            Vector3 v3 = vertices[triangles[i+2]];
            volume += get_signed_triangle_volume(v1, v2, v3);
        }
        return Mathf.Abs(volume);
    }

    private float get_signed_triangle_volume(Vector3 v1, Vector3 v2, Vector3 v3){
        float v321 = v3.x * v2.y * v1.z;
        float v231 = v2.x * v3.y * v1.z;
        float v312 = v3.x * v1.y * v2.z;
        float v132 = v1.x * v3.y * v2.z;
        float v213 = v2.x * v1.y * v3.z;
        float v123 = v1.x * v2.y * v3.z;
        float volume = (1.0f/6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
        return volume;
    }

}