﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevitationBehaviour : MonoBehaviour
{
    private ParticleSystem[] particles;
    private bool levitationOn = false;
    // Start is called before the first frame update
    void Start()
    {
        particles = this.GetComponentsInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        levitationOn = Input.GetKey("space");
        
        foreach(var x in particles){
                
                if(levitationOn) {
                    if(!x.isPlaying){
                        x.Play();
                    }                  
                }
                else{
                    if(x.isPlaying){
                        x.Stop();
                    }
                } 
            }
    }
}
