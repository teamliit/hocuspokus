﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// Applies a circular floating motion with real physics and randomness to a stationary 3D object
public class CircularFloatMotion : MonoBehaviour {

    public AnimationCurve curve;

	public float radius = 1f;
	public float rate = 1f;
	public float rateRandomness = 1f;
	 float yRange = 2f;
	private Rigidbody body;

	private Vector3 init;
	void Start () {

		init = transform.position;

		this.gameObject.AddComponent<Rigidbody>();
		body = this.gameObject.GetComponent<Rigidbody>();
		body.mass = 1f;
		body.drag = 0f;
		body.angularDrag = 0.05f;
		body.collisionDetectionMode = CollisionDetectionMode.Discrete;
		body.useGravity = false;

		body.AddTorque(MotionUtils.MkRandVec3());
		body.AddForce(MotionUtils.MkRandVec3());
		curve = new AnimationCurve(
			new Keyframe(0, rateRandomness * ( Random.value)), 
			new Keyframe(1, rateRandomness * ( Random.value)));
		curve.preWrapMode = WrapMode.ClampForever;
        curve.postWrapMode = WrapMode.ClampForever;
	}
	void Update () {
		 
		MotionUtils.RangedDeflect(init.y, yRange, transform.position.y, v => body.AddForce(new Vector3(0, v, 0)));
 
		var speed = 1f / (rate + curve.Evaluate((Time.time)));
		transform.position = new Vector3(
			(init.x ) + Mathf.Sin(Time.time * speed) * radius,
			transform.position.y,
			(init.z ) + Mathf.Cos(Time.time * speed) * radius
		); 
	}

}
