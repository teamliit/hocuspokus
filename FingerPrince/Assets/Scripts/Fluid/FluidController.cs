using libfluid;
using Optional;
using UnityEngine;

public class FluidController : MonoBehaviour {
    private FluidMesh system;
    private Option<FluidWell> well;

    
    
    private void Start() {
        var animator = gameObject.AddComponent<ValueAnimation>().Animator;
        

        system = FindObjectOfType<FluidMesh>();
        well = FindObjectOfType<FluidWell>().SomeNotNull();

        var drain = FindObjectOfType<FluidDrain>();

        animator.Untagged(100, 2.5f, 1f)
            .EvaluateWith(f => { drain.width = f; }).Submit();

//        var sources = FindObjectsOfType<FluidSource>();
//
//        float maxFlow = 15f;
//
//        animator.Untagged(maxFlow, maxFlow, 2.5f).EvaluateWith(f => {
//            foreach (var source in sources) {
//                source.rate = (long) f;
//            }
//        }).Submit(() => {
//            animator.Untagged(maxFlow, 0, 1).EvaluateWith(f => {
//                foreach (var source in sources) {
//                    source.rate = (long) f;
//                }
//            }).Submit();
//        });
    }


    private void Update() {
        well.MatchSome(x => {
            var activate = Input.GetKey("space");
            x.force = activate ? 0.0092f : 0;
        });
    }
}