using System;
using libfluid;
using UnityEngine;

public class FluidSourceController : MonoBehaviour {
    public float flow = 15f;
    private FluidQuery detector;
    private FluidSource source;
    private ValueAnimator animator;
    public GameObject container;
    public Vector3 localOffset;

    private void Start() {
        if (container != null)
            detector = FluidQuery.AffixTo(container, localOffset);
        else throw new Exception("contain was null!");

        animator = gameObject.AddComponent<ValueAnimation>().Animator;
        source = gameObject.GetComponent<FluidSource>();

//        animator.Untagged(0, flow, 2.5f)
//            .EvaluateWith(f => { source.rate = (long) f; })
//            .Submit(() => {
//                animator.Untagged(flow, 0, 1)
//                    .EvaluateWith(f => { source.rate = (long) f; })
//                    .Submit();
//            });
    }

    private void Update() {
        var N = detector.Neighbours().ValueOr(0);
        source.rate = N < 3 ? (long) flow : 0;
    }
}