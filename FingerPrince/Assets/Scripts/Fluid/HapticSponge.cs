using Optional;
using UnityEngine;

public class HapticSponge : MonoBehaviour {
    private HapticTool tool;

    private Collider bounds;

    private void Start() {
        tool = FindObjectOfType<HapticTool>();
        bounds = gameObject.GetComponent<Collider>();
    }

    private Option<Vector3> delta = Option.None<Vector3>();
    private Option<Vector3> lastPos = Option.None<Vector3>();

    private void FixedUpdate() {
        var that = tool.gameObject.transform.position;

        delta.MatchSome(x => {
            var moved = that + x;

            var pos = new Vector3(moved.x, transform.position.y, moved.z);
            pos = lastPos.Map(p => Vector3.Lerp(pos, p, Mathf.Clamp01(Vector3.Distance(p, pos) - 1f)))
                .ValueOr(pos);
            Debug.Log(pos);
            transform.position = pos;
            lastPos = pos.Some();
        });

        var closest = bounds.ClosestPoint(that);

        delta = Vector3.Distance(closest, that) > 0.4f
            ? Option.None<Vector3>()
            : (transform.position - closest).Some();
    }
}