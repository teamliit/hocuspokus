using libfluid;
using UnityEngine;

public class FixedFluidDispenser : MonoBehaviour {
    public float flow = 10f;
    public float duration = 2.5f;
    
    
    private void Start() {
        var animator = gameObject.AddComponent<ValueAnimation>().Animator;
        var source = gameObject.GetComponent<FluidSource>();

        animator.Untagged(0, flow, duration)
            .EvaluateWith(f => { source.rate = (long) f; })
            .Submit(() => {
                animator.Untagged(flow, 0, 1)
                    .EvaluateWith(f => { source.rate = (long) f; })
                    .Submit();
            });
    }
}