﻿using System.Collections;
using System.Collections.Generic;
using libfluid;
using UnityEngine;

public class HapticFluid : MonoBehaviour {

    [Range(2f, 10f)]
    public float viscosity;

    private FluidQuery query;

    void Start(){
        query = gameObject.AddComponent<FluidQuery>();
    }


    private void FixedUpdate()
    {
        
    }

    // check for contact with the object
    public bool Contact(Vector3 pos, Vector3 vel)
    {
        return query.Neighbours().HasValue;
    }
}