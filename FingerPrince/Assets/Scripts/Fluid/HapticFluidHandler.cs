﻿using System.Collections;
using System.Collections.Generic;
using libfluid;
using UnityEngine;

public class HapticFluidHandler : MonoBehaviour {
 
    // Simple moving average, otherwise known as a low pass filter
    private class VectorSMA {
        private Queue<Vector3> ns = new Queue<Vector3>();
        private Vector3 sigma = Vector3.zero;
        private int N;
        public readonly int window;

        public VectorSMA(int window) {
            this.window = window;
        }

        public void write(Vector3 v) {
            sigma += v;
            ns.Enqueue(v);
            if (ns.Count > window) {
                sigma -= ns.Dequeue();
            }
            else N++;
        }

        public Vector3 average() {
            return sigma / N;
        }
    }

    private Vector3 lastVelocity = Vector3.zero;
    private Vector3 lastPosition = Vector3.zero;
    private float viscosity = -1;
    private const float VELOCITY_LIMIT = 1.5f;

    IEnumerator BindHapticHook() {
        // wait for velocities to settle (initial vectors are too large to handle in a sensible way)  
        yield return new WaitForSeconds(2);
        VectorSMA sma = new VectorSMA(8);
        // NOTE: the forces returned in the callback is additive, meaning that the force gets added 
        // to what the physics engine (CHAI3D) has computed.
        // XXX: You can only have one callback AFAIK, we can always write a dispatcher to have more
        // than one behavior though
        UnityHaptics.SetHook((position, velocity) => {
            
            // NOTE: must return within <= 1ms (1Khz)
            lastVelocity = velocity;
            lastPosition = position;
            if (viscosity < 0) return Vector3.zero;
            sma.write(velocity);

            var result = - sma.average() / (viscosity * 10.5f); // -1 * average / viscosity_coefficient 
//            Debug.Log(result);

            return new Vector3(
                Mathf.Clamp(result.x, -VELOCITY_LIMIT, VELOCITY_LIMIT),
                Mathf.Clamp(result.y, -VELOCITY_LIMIT, VELOCITY_LIMIT),
                Mathf.Clamp(result.z, -VELOCITY_LIMIT, VELOCITY_LIMIT)
            );
        });
    }

    private FluidAggregateQuery query;
    void Start() {
        query = gameObject.AddComponent<FluidAggregateQuery>();
        StartCoroutine(BindHapticHook());
    }

    void Update() {
        
       
//        var hfs = FindObjectsOfType<HapticFluid>();
        // can't be functional, this needs to be really fast
        viscosity = -1;
        query.Neighbours.MatchSome(N => {
            if (N >= 2) {
                viscosity = Mathf.Clamp(N, 3f, 20f);
            }
        });
//        foreach (var hf in hfs) {
//            if (!hf.Contact(lastPosition, lastVelocity)) continue;
//            viscosity = hf.viscosity;
//            break;
//        }
    }

    void OnDisable() {
        // XXX UnityHaptics fails to release the hook safely; if left unreleased, 
        // unity would crash on the next game un
        if (HapticNativePlugin.remove_hook() != HapticNativePlugin.SUCCESS) {
            Debug.LogWarning("Remove hook failed, perhaps scene was terminated before bind");
        }
    }
}