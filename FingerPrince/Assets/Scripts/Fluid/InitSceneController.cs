using libfluid;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InitSceneController : MonoBehaviour {
    private FluidQuery flame;
    private FluidSource source;
    private ValueAnimator animator;
    private AsyncOperation loader;
    private RawImage blind;

    private void Start() {
        flame = FindObjectOfType<FluidQuery>();
        source = FindObjectOfType<FluidSource>();
        animator = gameObject.AddComponent<ValueAnimation>().Animator;

        source.rate = 0;
        source.velocity = new Vector3(0, 1.6f, 0);
        loader = SceneManager.LoadSceneAsync("gameScene");
        loader.allowSceneActivation = false;

        blind = FindObjectOfType<RawImage>();
        blind.enabled = false;
        blind.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
    }


    private bool done = false;

    private void Update() {
        if (done) return;
        var activate = Input.GetKey("space");

        source.rate = activate ? 50 : 0;


        if (flame.Neighbours().ValueOr(0) > 0) {
            done = true;
            Debug.Log("GO!!");

            blind.enabled = true;

            animator.Untagged(0, 1, 2f)
                .EvaluateWith(x => { blind.color = new Color(0, 0, 0, x); })
                .Submit(() => {
                    blind.color = Color.black;

                    animator.Untagged(0, 0, 0.5f)
                        .EvaluateWith(_ => { })
                        .Submit(() => { loader.allowSceneActivation = true; });
                });
        }
    }
}