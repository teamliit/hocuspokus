using System;
using System.Collections.Generic;
using System.Linq;
// using UnityEditor.Build.Content;
using UnityEngine;


public interface IInterpolator {
    float Curve(float t);
}

public static class Interpolators {
    public static readonly IInterpolator EASE_BOTH = new EaseBoth();
    public static readonly IInterpolator LINEAR = new Linear();

    private struct Linear : IInterpolator {
        public float Curve(float t) {
            return t;
        }
    }

    private struct EaseBoth : IInterpolator {
        public float Curve(float t) {
            return Mathf.Clamp01((t < 0.2f) ? 3.125f * t * t
                : (t > 0.8f) ? -3.125f * t * t + 6.25f * t - 2.125f
                : 1.25f * t - 0.125f);
        }
    }

    public struct Decelerate : IInterpolator {
        private float factor;

        public Decelerate(float factor) {
            this.factor = factor;
        }

        public float Curve(float t) {
            return Mathf.Approximately(factor, 1.0f)
                ? (1.0f - (1.0f - t) * (1.0f - t))
                : (1.0f - Mathf.Pow((1.0f - t), 2 * factor));
        }
    }
}

public class ValueAnimator {
    private static volatile int counter = 0;

    static string UniqueTag() {
        return $"Animation #{++counter}";
    }

    internal class State {
        private readonly float delta;
        private readonly float offset;
        private readonly float lengthInSeconds;
        private readonly float timeAtStart;
        private readonly Action<float> f;
        private readonly IInterpolator interpolator;
        public Action OnEnd { private get; set; } = () => { };


        private float now;

        public State(float start, float end, float lengthInSeconds, Action<float> f,
            IInterpolator interpolator) {
            this.delta = end - start;
            this.offset = start;
            this.lengthInSeconds = lengthInSeconds;
            this.f = f;
            this.interpolator = interpolator;
            this.timeAtStart = Time.time;
            this.now = this.timeAtStart;
        }

        public bool InterpolateNow() {
            now = Time.time;
            var elapsed = (now - timeAtStart);
            if (elapsed > lengthInSeconds) {
                OnEnd();
                return false;
            }

            var ratio = interpolator.Curve(elapsed / lengthInSeconds);
            f(offset + delta * ratio);
            return true;
        }
    }

    private Dictionary<string, State> animations = new Dictionary<string, State>();

    public void Tick() {
        var finished = new List<string>();
        var next = new Dictionary<string, State>(animations);
        foreach (var item in next) {
            if (!item.Value.InterpolateNow()) finished.Add(item.Key);
        }

        foreach (var k in finished) animations.Remove(k);
    }


    public class SubmitBuilder {
        private readonly Action<State> drain;
        private State state;

        internal SubmitBuilder(Action<State> drain, State state) {
            this.drain = drain;
            this.state = state;
        }

        /// <summary>
        /// Submit animation for immediate playback 
        /// </summary>
        /// <param name="endActions">a list of actions that would run in order after the animation is completed</param>
        public void Submit(params Action[] endActions) {
            state.OnEnd = () => {
                foreach (var action in endActions) action();
            };
            drain(state);
        }
    }

    public class AnimationBuilder {
        private readonly Action<State> drain;
        private float start;
        private float end;
        private float lengthInSeconds;

        internal AnimationBuilder(Action<State> drain,
            float start, float end, float lengthInSeconds) {
            this.drain = drain;
            this.start = start;
            this.end = end;
            this.lengthInSeconds = lengthInSeconds;
        }

        public SubmitBuilder EvaluateWith(Action<float> f, IInterpolator interpolator) {
            return new SubmitBuilder(drain,
                new State(start, end, lengthInSeconds, f, interpolator));
        }

        public SubmitBuilder EvaluateWith(Action<float> f) {
            return EvaluateWith(f, Interpolators.LINEAR);
        }
    }

    public bool Cancel(string tag) {
        return animations.Remove(tag);
    }

    /// <summary>
    /// Create an untagged animation that would run until completion
    /// </summary>
    public AnimationBuilder Untagged(float start, float end, float lengthInSeconds) {
        return Tagged(UniqueTag(), start, end, lengthInSeconds);
    }


    /// <summary>
    /// Create a tagged animation that could be removed using <see cref="Cancel"/>
    /// </summary>
    public AnimationBuilder Tagged(string tag, float start, float end, float lengthInSeconds) {
        return new AnimationBuilder(s => animations.Add(tag, s), start, end, lengthInSeconds);
    }
}