﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using UnityEngine;

public class LevitationEnabler : MonoBehaviour {
    private bool lastInput;
    private GlowBehavior glow;
    private HapticMagnet[] xs;

    void Start() {
        lastInput = false;
        glow = gameObject.AddComponent<GlowBehavior>();
        glow._renderer = GetComponent<Renderer>();
        glow.maxIntensity = 5f;
        xs = GameObject.FindObjectsOfType<HapticMagnet>();
    }

    void Update() {
        glow.Update();

        bool input = Input.GetKey("space");

        // Glow should be triggered any time space is held down
        if (input) {
            glow.Trigger();
        }

        // Magnetise and demagnitise are toggles, so we only trigger them
        // when the last input was different to the current one, ie. when
        // the input has been pressed or released
        if (input & !lastInput) {
            foreach (var x in xs) {
                x.magnitise();
            }        
        }
        else if (!input & lastInput) {
            foreach (var x in xs) {
                x.demagnitise();
            }
        }

        lastInput = input;
    }
}