
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Optional;
using Optional.Linq;
using System.Text;

// Each level is a recipe that the player needs to complete
public class Recipe
{
    public float baseScore;
    public int totalScore;
    private float startTime, playTime;
    public float maxTime;
    public static Color Orange = new Color(1f, 0.65f, 0f);
    public static Color Purple = new Color(0.58f, 0f, 0.83f);
    // public static Color Green = new Color(0f, 0.73f, 0.12f);

    public enum Feel
    {
        Squishy, Hard
    }

    public enum Texture
    {
        Rough, Smooth
    }

    public enum Shape
    {
        Cube, Sphere
    }


    public class Requirement
    {

        public readonly ClosedColour colour;
        public readonly Option<Feel> feel;
        public readonly Option<Texture> texture;
        public readonly Shape shape;
        public readonly int score;
        public bool fulfilled;
        

        public Requirement(int score, ClosedColour colour, Shape shape, Option<Feel> feel, Option<Texture> texture)
        {
            this.score = score;
            this.colour = colour;
            this.shape = shape;
            this.feel = feel;
            this.texture = texture;
        }

        public override string ToString(){
            return $"Requirement({colour}, {shape}, {feel}, {texture})";
        }

    }

    public readonly IList<Requirement> todos;

    public Recipe(float recipeScore, float Tmax, params Requirement[] values)
    {   
        todos = new List<Requirement>(values);
        baseScore = recipeScore;
        maxTime = Tmax;
    }

    static string implode<T>( IList<T> xs)  where T : class{ 
        StringBuilder builder = new StringBuilder("[");
        
        foreach (var x in xs)
        {
            builder.Append(x.ToString());    
            if(x != xs.Last()) builder.Append(", ");    
        }

        builder.Append("]");
        return builder.ToString();
    }

    // the user trys to fulfill the requirements by throwing ingredients into the cauldron, 
    // this gets called to check whether the ingredient fulfills any of the requirement
    public Option<Requirement> Fulfill(Ingredient that)
    {

        Debug.Log("Fulfil called, need: " +  implode(todos) + "; got " + that.ToString());

        return todos.FirstOrDefault(todo =>
            todo.shape == that.shape &&
            that.colour.Contains(todo.colour) &&
            // so that ingredient either satisifies the specified constraint or pass anyway
            //  if the todo does not impose a constrant
            // Scala pattern:  case Some(`that.foo`) | None => true 
            todo.texture.Map(t => t == that.texture).ValueOr(true) &&
            todo.feel.Map(t => t == that.feel).ValueOr(true)
        ).SomeNotNull();
        // return Option.None<Requirement>();
    }

    // public void DecrementScore(){
    //     if(score > 0) score -= Time.deltaTime;
    //     else if(score < 0) score = 0;
    // }

    public void startTimer(){
        startTime = Time.time;
    }
    public void stopTimer(){
        playTime = Time.time - startTime;
    }
    public int calcScore(){
        float bonus = calcBonus();
        int total = (int) Math.Floor(baseScore + bonus);
        Debug.Log(total);
        return total;
    }
    //Calculating the bonus score according to the time taken for each recipe
    public int calcBonus(){
        float ratio = playTime/ maxTime;
        Debug.Log("ratio: "+ ratio);
        Debug.Log("baseScore: " + baseScore);
        int bonus = (int) Math.Floor(baseScore + ratio * baseScore);
        return bonus; 
    }
}