﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyOutMethods : MonoBehaviour
{
    public enum MethodNames
    {
        ChooseRandomMethod,
        FloatThenShoot,
        VibrateOut,
        ElasticShoot
    }
}
