using UnityEngine;

public class ValueAnimationSample : MonoBehaviour {
    void Start() {
        
        // add your animator, one animator can handle unlimited concurrent animations
        var animator = gameObject.AddComponent<ValueAnimation>().Animator;

        
        animator.Untagged(0, 3, 1).EvaluateWith(f => {
            // the float value will be between 0 to 3 in this lambda expr
            transform.position = new Vector3(f, 0, 0);
        }, Interpolators.EASE_BOTH).Submit(() => {
            // start another one at the end
            animator.Untagged(3, 0, 1).EvaluateWith(f => {
                transform.position = new Vector3(f, 0, 0);
            }, new Interpolators.Decelerate(3f)).Submit();
        });
        
        
    }
}