using System.Collections.Generic;
using Sys = System;
using UnityEngine.UI;
using UnityEngine;
using Optional;
using System.Collections;



// This manages all the requirement things in a recipe
public class GameManager : MonoBehaviour
{

    private int score = 0;
    private float startTime;
    private float maxTime = 500;
    private bool timing = false;
    private bool end = false;
    private ValueAnimator valueAnimator;
    Text text; // For score

    public IList<Recipe> Recipes { get; } = new List<Recipe> {
        new Recipe(
            100,
            150,
            new Recipe.Requirement(50, ClosedColour.Red, Recipe.Shape.Cube, Recipe.Feel.Hard.None(),
                Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Blue, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Green, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        ),
        new Recipe(
            100,
            180,
            new Recipe.Requirement(50, ClosedColour.Green, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(80, ClosedColour.Yellow, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Red, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        ),
        new Recipe(
            100,
            180,
            new Recipe.Requirement(80, ClosedColour.Magenta, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Red, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Blue, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        ),
        new Recipe(
            100,
            180,
            new Recipe.Requirement(50, ClosedColour.Blue, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(50, ClosedColour.Green, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(80, ClosedColour.Cyan, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        ),
        new Recipe(
            100,
            240,
            new Recipe.Requirement(80, ClosedColour.Magenta, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(80, ClosedColour.Cyan, Recipe.Shape.Cube,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None()),
            new Recipe.Requirement(80, ClosedColour.Yellow, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        ),
        new Recipe(
            100,
            100,
            new Recipe.Requirement(100, ClosedColour.Black, Recipe.Shape.Sphere,
                Recipe.Feel.Hard.None(), Recipe.Texture.Rough.None())
        )
    };

    private GameObject AddIngredient(Recipe.Shape shape, Recipe.Texture texture, Recipe.Feel feel, Vector3 pos)
    {

        PrimitiveType that;
        switch (shape)
        {
            case Recipe.Shape.Cube: that = PrimitiveType.Cube; break;
            case Recipe.Shape.Sphere: that = PrimitiveType.Sphere; break;
            default: that = PrimitiveType.Cube; break;
        }

        GameObject obj = GameObject.CreatePrimitive(that);
        var ingredient = obj.AddComponent<Ingredient>();
        ingredient.shape = shape;
        ingredient.texture = texture;
        ingredient.feel = feel;
        obj.transform.position = pos;

        Rigidbody rb = obj.AddComponent<Rigidbody>();
        rb.useGravity = true;
        return obj;
    }

    //Finding the current recipe
    public Option<Recipe> currentRecipe() {
        if (Recipes.Count <= 0) {
            return Option.None<Recipe>();
        } else {
            return Option.Some<Recipe>(Recipes[0]);
        }
    }

    public void FulfilledReq(Recipe.Requirement req) {
        currentRecipe()
        .MatchSome( recipe => {
            addScore(req.score);
            tutorial.CrossOff(req);
            recipe.todos.Remove(req);
            if (recipe.todos.Count == 0) {
                recipe.stopTimer();
                float bonus = recipe.calcBonus();   
                valueAnimator.Untagged(0, 0, 1.5f).EvaluateWith(f => {
                    text.text = "Bonus: +" + ((int)bonus).ToString();
                    text.color = new Vector4 (0.2f, 0, 0.2f, text.color.a);
                }).Submit(()=>{
                    addScore(recipe.calcBonus());
                    text.color = new Vector4 (0, 0, 0, text.color.a);
                });
                
                tutorial.clearText();
                Recipes.RemoveAt(0);
                //Dispaly win screen and highest score and players fulfill all the recipes
                if (Recipes.Count == 0) {
                    tutorial.winScreen();
                    int highest = (score > PlayerPrefs.GetInt("highscore", 0)) ? score : PlayerPrefs.GetInt("highscore", 0);
                    Debug.Log( "stored highest: " + PlayerPrefs.GetInt("highscore", 0));
                    Debug.Log("highest: " + highest);
                    text.text = "Score: " + score.ToString() + "\n Highest: " + highest.ToString() + "\n Time Left: " + Mathf.Round(maxTime - (Time.time - startTime)) + " secs";
                    PlayerPrefs.SetInt("highscore", highest);
                    PlayerPrefs.Save();
                }
                //Move on to the next recipe when there are still recipes left
                else {
                    tutorial.level++;
                    tutorial.spawnTexts();
                };
                
            }
        });
    }

    //Update the scoreboard
    private void addScore(int deltaScore) { 
        score += deltaScore;
        int highest = (score > PlayerPrefs.GetInt("highscore", 0)) ? score : PlayerPrefs.GetInt("highscore", 0);
        text.text =  "Score: " + score.ToString() + "\n Highest: " + highest.ToString() + "\n Time Left: " + Mathf.Round(maxTime - (Time.time - startTime)) + " secs";
        
    }

    //Initialize the scoreboard display
    private void cardPopup(){ 
        GameObject plane = GameObject.FindWithTag("scoreBoard");
        GameObject cardCanvas = new GameObject();
        cardCanvas.name = "canvas";
        cardCanvas.AddComponent<Canvas>();
        cardCanvas.AddComponent<CanvasScaler>();
        cardCanvas.AddComponent<GraphicRaycaster>();
        cardCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        cardCanvas.GetComponent<RectTransform>().SetParent(plane.transform);
        cardCanvas.GetComponent<RectTransform>().sizeDelta = new Vector3(20f ,12f, 0);
        cardCanvas.GetComponent<RectTransform>().localEulerAngles = new Vector3(90,0,0);
        cardCanvas.GetComponent<RectTransform>().localPosition = new Vector3(0.02f, 0.003f,-0.055f);
        cardCanvas.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 100;

        GameObject textGO = new GameObject();
        textGO.transform.parent = cardCanvas.transform;
        textGO.name = "score";
        text = textGO.AddComponent<Text>();
        text.font = Resources.Load<Font>("GOTHUT");
        text.text = "";
        text.fontStyle = FontStyle.Bold;
        text.lineSpacing = 0.6f; 
        text.alignment = TextAnchor.MiddleCenter;

        Color c = Color.black;
        c.a -= 0.1f;
        text.color = c;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;
        text.GetComponent<RectTransform>().sizeDelta = new Vector3(20f, 12f, 1);
        text.GetComponent<RectTransform>().localPosition = new Vector3(0.02f,0,-0.06f);
        text.GetComponent<RectTransform>().localScale = new Vector3(1.15f, 1.15f, 1);
    }

    private static T RandomEnum<T>(Sys.Type type) where T : struct, Sys.IConvertible
    {
        var len = Sys.Enum.GetValues(type).Length;
        return (T)Sys.Enum.GetValues(type).GetValue(Random.Range(0, len - 1));
    }

    public void startGame() {
        cardPopup();
    }
    
    //This is called when time is up
    public void endGame() {
        tutorial.clearText();
        tutorial.gameOver();
        Vector3 objPos = GameObject.FindWithTag("scoreBoard").transform.position;
 
        float tz = 45f;
        float ty = 5f;
        valueAnimator.Untagged(0, 1, 3).EvaluateWith(f => {
            var y = objPos.y - f*ty;
            var z = objPos.y - f*tz;
            GameObject.FindWithTag("scoreBoard").transform.position = new Vector3 (objPos.x, y, z);
        }).Submit();
       
        PlayerPrefs.SetInt("highscore", score);
        PlayerPrefs.Save();
    }
    private Tutorial tutorial;

    void Start() {

        tutorial = FindObjectOfType<Tutorial>();

        startGame(); 
        valueAnimator = gameObject.AddComponent<ValueAnimation>().Animator;
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        // TODO add the right amount of ingredients so that the game can be completed
        // TODO or just add more ingredients at runtime on demand
        // for (int i = 0; i < 5; i++)
        // {
        //     AddIngredient(
        //         RandomEnum<Recipe.Shape>(typeof(Recipe.Shape)),
        //         RandomEnum<Recipe.Texture>(typeof(Recipe.Texture)),
        //         RandomEnum<Recipe.Feel>(typeof(Recipe.Feel)),
        //         new Vector3(Random.Range(-10.0f, 0), 0, Random.Range(0, 10.0f))
        //     );
        // }
    }
    void Update(){
        int highest = (score > PlayerPrefs.GetInt("highscore", 0)) ? score : PlayerPrefs.GetInt("highscore", 0);
        if (tutorial.level > 0 && !timing) {
            timing = true;
            startTime = Time.time;
        }
        //Updating the timer
        if (Time.time - startTime >= maxTime && !end){
            end = true;
            endGame();
            text.text = "Score: " + score.ToString() + "\n Highest: " + highest.ToString() + "\n Time Left: 0";
        } else if (tutorial.level > 0) {
            var remaining = Mathf.Round(Mathf.Clamp(maxTime - (Time.time - startTime), 0, 1048576));  
            text.text = "Score: " + score.ToString() + "\n Highest: " + highest.ToString() + "\n Time Left: " + remaining;
        }

    }

}