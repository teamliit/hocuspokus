﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatThenShoot : FlyOutMethods
{
    private Rigidbody rb;
    private float time = 0f;

    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
        rb = GetComponent<Rigidbody>();

        rb.AddForce(new Vector3(0, 1500, 0));    //Make object float up for awhile
    }

    //Shoot out in random directions
    void Shoot()
    {
        rb.velocity = Vector3.zero;
        Vector3 randomDir = new Vector3(Random.Range(-1f, 1f), Random.Range(0f, 0.2f), Random.Range(-1f, 1f)).normalized;
       
        rb.velocity = randomDir * 20;

        Destroy(GetComponent<FloatThenShoot>());
    }

 
    void Update()
    {
        //Rotate while floating up
        if (time < 2f)
        {
            Vector3 randomAxis = new Vector3(Random.value, Random.value, Random.value);
            transform.Rotate(randomAxis, Time.deltaTime * 100);
            time += Time.deltaTime;
        }
        else Shoot();             
    }
}
