using System;
using UnityEngine;
public static class MotionUtils{

    // apply the action with a delta that brings the actual value closer to the range
    public static void RangedDeflect(float value, float range, float actual,  Action<float> f){
		var min = value - range / 2;
		var max = value + range / 2;
		if(actual < min) f(range / 2);
		else if(actual > max) f(-range / 2);
	}

    public static Vector3 MkRandVec3(float scale = 1f) {
        return new Vector3(
            UnityEngine.Random.value * scale,
            UnityEngine.Random.value * scale, 
            UnityEngine.Random.value * scale);
    }

}