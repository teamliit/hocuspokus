﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDisable : MonoBehaviour {

	public float secondsBeforeHide = 1f;
	private float shown = -1f;

	void Update () {
		if(gameObject.activeSelf && shown < 0) { 
			shown = Time.time; 			 
		} else if(shown >= 0 && (Time.time - shown >= secondsBeforeHide)){
			gameObject.SetActive(false);
		}
	}
}
