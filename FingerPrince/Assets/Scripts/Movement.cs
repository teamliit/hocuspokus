﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used for testing interactions
public class Movement : MonoBehaviour {
	public float speed = 1f;

	public ForceMode forceMode = ForceMode.Force;

	private float moveX;

	private float moveY;

	Rigidbody rigidBody;

	// Use this for initialization
	void Start () {
		rigidBody = this.GetComponent<Rigidbody>();

		if (rigidBody == null) {
			Debug.LogError("Rigid body could not be found");
		}
	}
	
	// Update is called once per frame
	void Update () {
		moveX = Input.GetAxis("Horizontal");
		moveY = Input.GetAxis("Vertical");
	}

	void FixedUpdate () {

		if (rigidBody != null) {
			Vector3 moveVector = new Vector3(moveX, 0, moveY) * speed;
			rigidBody.AddForce(moveVector, forceMode);
		}
	}
}
