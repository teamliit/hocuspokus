

Setup for Linux:


Be aware that versions absolutely have to match the description otherwise Unity will crash sooner or later. 

Install Unity 2018.2.14f1. Unity does not post installer URLs for Linux anywhere as it seems to be in perpetual beta. The following URL is extracted from ArchLinux's AUR:

    https://download.unity3d.com/download_unity/3262fb3b0716/LinuxEditorInstaller/Unity.tar.xz

Checksum is `13b41167bdc6453326a0594d04557a18f4888aa4`
The relevant AUR commit can be found at [https://aur.archlinux.org/cgit/aur.git/commit/?h=unity-editor&id=2e64c6369151cd36e3281ef985e9554c2a8d5941]


Extract and install the tarball.

Install VisualStudio Code using your distro's package manager, also make sure basic build tools such as `make` and `clang` are installed(e.g `sudo apt install build-essentials`)

Install .NET SDK by following [https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial]

For IntelliSense to work, install `mono-devel`, this contains the .NETFramework 3.5 that is used by Unity.  

